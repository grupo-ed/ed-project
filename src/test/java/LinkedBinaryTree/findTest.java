package LinkedBinaryTree;

import Exceptions.NotFoundException;
import Tree.Heap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.management.InstanceNotFoundException;

public class findTest {

    /**
     * Testing Class
     */
    private Heap<String> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new Heap<>();
    }

    /**
     * Id: findTest01
     * Pré-condição: LinkedBinaryTree vazia
     * Input: "2"
     * Pós-Condição: NotFoundException
     * Tipo: Causa-Efeito
     */
    @Test
    public void findTest01(){
        Assertions.assertThrows(NotFoundException.class,()->testSubject.find("2"));
    }

    /**
     * Id: findTest02
     * Pré-condição: LinkedBinaryTree com elementos Front , 1 , 2
     * Input: "2"
     * Pós-Condição: "2"
     * Tipo: Causa-Efeito
     */
    @Test
    public void containsTest02() throws NotFoundException, InstanceNotFoundException {
        testSubject.addElement("1");
        testSubject.addElement("2");
        testSubject.addElement("Front");
        Assertions.assertEquals("2",testSubject.find("2"));
    }

    /**
     * Id: findTest03
     * Pré-condição: LinkedBinaryTree com elementos Front , 1 , 2
     * Input: "3"
     * Pós-Condição: NotFoundException
     * Tipo: Causa-Efeito
     */
    @Test
    public void containsTest03() throws InstanceNotFoundException {
        testSubject.addElement("1");
        testSubject.addElement("2");
        testSubject.addElement("Front");
        Assertions.assertThrows(NotFoundException.class, ()->testSubject.find("3"));
    }

    /**
     * Id: findTest06
     * Pré-condição: LinkedBinaryTree com elementos Front , 1 , 2
     * Input: null
     * Pós-Condição: {@link NullPointerException}
     * Tipo: Causa-Efeito
     */
    @Test
    public void containsTest06() throws InstanceNotFoundException {
        testSubject.addElement("1");
        testSubject.addElement("2");
        testSubject.addElement("Front");
        Assertions.assertThrows(NullPointerException.class, ()->testSubject.find(null));
    }
}
