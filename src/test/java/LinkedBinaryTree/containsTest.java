package LinkedBinaryTree;

import Tree.Heap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.management.InstanceNotFoundException;

public class containsTest {

    /**
     * Testing Class
     */
    private Heap<String> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new Heap<>();
    }

    /**
     * Id: containsTest01
     * Pré-condição: LinkedBinaryTree vazia
     * Input: "2"
     * Pós-Condição: false
     * Tipo: Causa-Efeito
     */
    @Test
    public void containsTest01(){
        Assertions.assertEquals(false,testSubject.contains("2"));
    }

    /**
     * Id: containsTest02
     * Pré-condição: LinkedBinaryTree com elementos Front , 1 , 2
     * Input: "2"
     * Pós-Condição: true
     * Tipo: Causa-Efeito
     */
    @Test
    public void containsTest02() throws InstanceNotFoundException {
        testSubject.addElement("1");
        testSubject.addElement("2");
        testSubject.addElement("Front");
        Assertions.assertEquals(true,testSubject.contains("2"));
    }

    /**
     * Id: containsTest03
     * Pré-condição: LinkedBinaryTree com elementos Front , 1 , 2
     * Input: "3"
     * Pós-Condição: false
     * Tipo: Causa-Efeito
     */
    @Test
    public void containsTest03() throws InstanceNotFoundException {
        testSubject.addElement("1");
        testSubject.addElement("2");
        testSubject.addElement("Front");
        Assertions.assertEquals(false,testSubject.contains("3"));
    }

    /**
     * Id: containsTest06
     * Pré-condição: LinkedBinaryTree com elementos Front , 1 , 2
     * Input: null
     * Pós-Condição: {@link NullPointerException}
     * Tipo: ECP/BVA
     */
    @Test
    public void containsTest06() throws InstanceNotFoundException {
        testSubject.addElement("1");
        testSubject.addElement("2");
        testSubject.addElement("Front");
        Assertions.assertFalse(testSubject.contains(null));
    }
}
