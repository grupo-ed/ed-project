package LinkedBinaryTree;

import Lists.ArrayUnorderedList;
import Tree.Heap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.management.InstanceNotFoundException;
import java.util.Iterator;

public class iteratorPreOrderTest {

    /**
     * Testing Class
     */
    private Heap<Integer> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp() throws InstanceNotFoundException {
        testSubject = new Heap<>();
        testSubject.addElement(8);
        testSubject.addElement(1);
        testSubject.addElement(3);
        testSubject.addElement(2);
    }

    /**
     * Id: iteratorPreOrderTest01
     * Pré-condição: LinkedBinaryTree com elementos 8,1,3,2
     * Input: N/A
     * Pós-Condição: 1,2,8,3
     * Tipo: Causa-Efeito
     */
    @Test
    public void iteratorPreOrderTest01(){
        ArrayUnorderedList<Integer> tempList = new ArrayUnorderedList<>();
        tempList.addToRear(1);
        tempList.addToRear(2);
        tempList.addToRear(8);
        tempList.addToRear(3);
        Iterator expected = tempList.iterator();
        Iterator atual = testSubject.iteratorPreOrder();
        while(expected.hasNext() && atual.hasNext()){
            Assertions.assertEquals(expected.next(),atual.next());
        }
    }
}
