package LinkedBinaryTree;

import Lists.ArrayUnorderedList;
import Tree.Heap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.management.InstanceNotFoundException;
import java.util.Iterator;

public class iteratorPostOrderTest {

    /**
     * Testing Class
     */
    private Heap<Integer> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp() throws InstanceNotFoundException {
        testSubject = new Heap<>();
        testSubject.addElement(8);
        testSubject.addElement(1);
        testSubject.addElement(3);
        testSubject.addElement(2);
    }

    /**
     * Id: iteratorPostOrderTest01
     * Pré-condição: LinkedBinaryTree com elementos 8,1,3,2
     * Input: N/A
     * Pós-Condição: 8,2,3,1
     * Tipo: Causa-Efeito
     */
    @Test
    public void iteratorPostOrderTest01(){
        ArrayUnorderedList<Integer> tempList = new ArrayUnorderedList<>();
        tempList.addToRear(8);
        tempList.addToRear(2);
        tempList.addToRear(3);
        tempList.addToRear(1);
        Iterator expected = tempList.iterator();
        Iterator atual = testSubject.iteratorPostOrder();
        while(expected.hasNext() && atual.hasNext()){
            Assertions.assertEquals(expected.next(),atual.next());
        }
    }
}
