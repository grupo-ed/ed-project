package LinkedBinaryTree;

import Tree.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LinkedBinaryTreeTest {

    /**
     * Id: LinkedBinaryTreeTest01
     * Pré-condição: N/A
     * Input: 2
     * Pós-Condição: N/A
     * Tipo: ECP
     */
    @Test
    public void LinkedBinaryTreeTest01(){
        Assertions.assertDoesNotThrow(()->new LinkedBinaryTree<Integer>(2));
    }

    /**
     * Id: LinkedBinaryTreeTest02
     * Pré-condição: N/A
     * Input: null
     * Pós-Condição: NullPointerException
     * Tipo: ECP
     */
    @Test
    public void LinkedBinaryTreeTest02(){
        Assertions.assertThrows(NullPointerException.class, ()->new LinkedBinaryTree<Integer>(null));
    }

    /**
     * Id: LinkedBinaryTreeTest05
     * Pré-condição: N/A
     * Input: 2,null,{@link LinkedBinaryTree}
     * Pós-Condição: N/A
     * Tipo: ECP
     */
    @Test
    public void LinkedBinaryTreeTest05(){
        Assertions.assertDoesNotThrow(()->new LinkedBinaryTree<Integer>(2, null, new LinkedBinaryTree<Integer>(2)));
    }

    /**
     * Id: LinkedBinaryTreeTest06
     * Pré-condição: N/A
     * Input: 2,{@link LinkedBinaryTree},null
     * Pós-Condição: N/A
     * Tipo: ECP
     */
    @Test
    public void LinkedBinaryTreeTest06(){
        Assertions.assertDoesNotThrow(()->new LinkedBinaryTree<Integer>(2, new LinkedBinaryTree<Integer>(2),null));
    }
}
