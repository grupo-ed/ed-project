package Graph;

import Exceptions.EmptyCollectionException;
import Exceptions.InvalidIndexException;
import Graphs.Graph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

public class iteratorDFSTest {
    /**
     * Testing Class
     */
    private Graph<Integer> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new Graph<>();
    }

    /**
     * Id: iteratorDFSTest03
     * Pré-condição: Coleção vazia
     * Input: N/A
     * Pós-Condição: Iterador vazio
     * Tipo: Causa-Efeito
     */
    @Test
    public void iteratorDFSTest03() throws EmptyCollectionException {
        Assertions.assertEquals(false, testSubject.iteratorDFS(1).hasNext());
    }

    /**
     * Id: iteratorDFSTest04
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: Iterador vazio
     * Tipo: Causa-Efeito
     */
    @Test
    public void iteratorDFSTest04() throws EmptyCollectionException {
        for(int i=2;i<=11;i++){
            testSubject.addVertex(i);
        }

        Assertions.assertEquals(false, testSubject.iteratorDFS(1).hasNext());
    }

    /**
     * Id: iteratorDFSTest05
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: Iterador BFS
     * Tipo: Causa-Efeito
     */
    @Test
    public void iteratorDFSTest05() throws EmptyCollectionException, InvalidIndexException {
        testSubject.addVertex(1);
        for(int i=2;i<=10;i++){
            testSubject.addVertex(i);
            testSubject.addEdge(i-1,i);
        }

        testSubject.addVertex(11);
        testSubject.addEdge(1,11);

        Iterator iterator = testSubject.iteratorDFS(1);

        Assertions.assertEquals(1,iterator.next());
        for(int i=2;i<=10;i++){
            Assertions.assertEquals(i,iterator.next());
        }
        Assertions.assertEquals(11,iterator.next());
        Assertions.assertEquals(false,iterator.hasNext());
    }
}
