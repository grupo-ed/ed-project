package Graph;

import Exceptions.EmptyCollectionException;
import Exceptions.InvalidIndexException;
import Graphs.Graph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

public class addEdgeTest {
    /**
     * Testing Class
     */
    private Graph<Integer> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new Graph<>();
    }

    /**
     * Id: addEdgeTest03
     * Pré-condição: Coleção vazia
     * Input: N/A
     * Pós-Condição: InvalidIndexException
     * Tipo: Causa-Efeito
     */
    @Test
    public void addEdgeTest03(){
        Assertions.assertThrows(InvalidIndexException.class, ()->testSubject.addEdge(1,2));
    }

    /**
     * Id: addEdgeTest04
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: InvalidIndexException
     * Tipo: Causa-Efeito
     */
    @Test
    public void addEdgeTest04(){
        for(int i=4;i<14;i++){
            testSubject.addVertex(i);
        }

        Assertions.assertThrows(InvalidIndexException.class, ()->testSubject.addEdge(1,2));
    }

    /**
     * Id: addEdgeTest05
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: Aresta adicionada
     * Tipo: Causa-Efeito
     */
    @Test
    public void addEdgeTest05() throws EmptyCollectionException {
        for(int i=1;i<10;i++){
            testSubject.addVertex(i);
        }

        Assertions.assertDoesNotThrow(()->testSubject.addEdge(1,2));

        Iterator iterator = testSubject.iteratorBFS(1);

        Assertions.assertEquals(1, iterator.next());
        Assertions.assertEquals(2, iterator.next());
        Assertions.assertEquals(false, iterator.hasNext());
    }
}
