package Graph;

import Exceptions.EmptyCollectionException;
import Graphs.Graph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class addVertexTest {
    /**
     * Testing Class
     */
    private Graph<Integer> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new Graph<>();
    }

    /**
     * Id: addVertexTest03
     * Pré-condição: Coleção vazia
     * Input: N/A
     * Pós-Condição: Vertice adicionado
     * Tipo: Causa-Efeito
     */
    @Test
    public void addVertexTest03(){
        Integer vertex = 1;
        Assertions.assertDoesNotThrow(()->testSubject.addVertex(vertex));

        Object[] vertices = testSubject.getVertices();
        Assertions.assertEquals(vertex, vertices[0]);

        for(int i=1;i<vertices.length-1;i++){
            Assertions.assertNull(vertices[i]);
        }
    }

    /**
     * Id: addVertexTest04
     * Pré-condição: Coleção com elementos
     * Input: N/A
     * Pós-Condição: Vertice adicionado
     * Tipo: Causa-Efeito
     */
    @Test
    public void addVertexTest04(){
        for(int i=0;i<=4;i++){
            testSubject.addVertex(i);
        }

        Integer vertex = 1;
        Assertions.assertDoesNotThrow(()->testSubject.addVertex(vertex));

        Object[] vertices = testSubject.getVertices();

        for(int i=0;i<=4;i++){
            Assertions.assertEquals(i,vertices[i]);
        }

        Assertions.assertEquals(1,vertices[5]);

        for(int i=6;i<vertices.length-1;i++){
            Assertions.assertNull(vertices[i]);
        }

    }

    /**
     * Id: addVertexTest05
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: Vertice adicionado
     * Tipo: Causa-Efeito
     */
    @Test
    public void addVertexTest05(){
        for(int i=0;i<=9;i++){
            testSubject.addVertex(i);
        }

        Integer vertex = 1;
        Assertions.assertDoesNotThrow(()->testSubject.addVertex(vertex));

        Object[] vertices = testSubject.getVertices();

        for(int i=0;i<=9;i++){
            Assertions.assertEquals(i,vertices[i]);
        }

        Assertions.assertEquals(1,vertices[10]);

        for(int i=11;i<vertices.length-1;i++){
            Assertions.assertNull(vertices[i]);
        }
    }

}
