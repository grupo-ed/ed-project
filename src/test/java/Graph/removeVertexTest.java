package Graph;

import Exceptions.InvalidIndexException;
import Graphs.Graph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class removeVertexTest {
    /**
     * Testing Class
     */
    private Graph<Integer> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new Graph<>();
    }

    /**
     * Id: removeVertexTest03
     * Pré-condição: Coleção vazia
     * Input: N/A
     * Pós-Condição: InvalidIndexException
     * Tipo: Causa-Efeito
     */
    @Test
    public void removeVertexTest03(){
        Assertions.assertThrows(InvalidIndexException.class, ()->testSubject.removeVertex(1));
    }

    /**
     * Id: removeVertexTest04
     * Pré-condição: Coleção com elementos
     * Input: N/A
     * Pós-Condição: InvalidIndexException
     * Tipo: Causa-Efeito
     */
    @Test
    public void removeVertexTest04(){
        for(int i=2;i<=9;i++){
            testSubject.addVertex(i);
        }

        Assertions.assertThrows(InvalidIndexException.class, ()->testSubject.removeVertex(1));
    }

    /**
     * Id: removeVertexTest05
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: Vertice removido
     * Tipo: Causa-Efeito
     */
    @Test
    public void removeVertexTest05(){
        for(int i=1;i<=10;i++){
            testSubject.addVertex(i);
        }

        Assertions.assertDoesNotThrow(()->testSubject.removeVertex(1));

        Object[] vertices = testSubject.getVertices();

        for(int i=0;i<vertices.length;i++){
            Assertions.assertNotEquals(1,vertices[i]);
        }
    }
}
