package Graph;

import Exceptions.EmptyCollectionException;
import Exceptions.InvalidIndexException;
import Graphs.Graph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

public class iteratorBFSTest {
    /**
     * Testing Class
     */
    private Graph<Integer> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new Graph<>();
    }

    /**
     * Id: iteratorBFSTest03
     * Pré-condição: Coleção vazia
     * Input: N/A
     * Pós-Condição: Iterador vazio
     * Tipo: Causa-Efeito
     */
    @Test
    public void iteratorBFSTest03() throws EmptyCollectionException {
        Assertions.assertEquals(false, testSubject.iteratorBFS(1).hasNext());
    }

    /**
     * Id: iteratorBFSTest04
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: Iterador vazio
     * Tipo: Causa-Efeito
     */
    @Test
    public void iteratorBFSTest04() throws EmptyCollectionException {
        for(int i=2;i<=11;i++){
            testSubject.addVertex(i);
        }

        Assertions.assertEquals(false, testSubject.iteratorBFS(1).hasNext());
    }

    /**
     * Id: iteratorBFSTest05
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: Iterador BFS
     * Tipo: Causa-Efeito
     */
    @Test
    public void iteratorBFSTest05() throws EmptyCollectionException, InvalidIndexException {
        testSubject.addVertex(1);
        for(int i=2;i<=10;i++){
            testSubject.addVertex(i);
            testSubject.addEdge(i-1,i);
        }

        testSubject.addVertex(11);
        testSubject.addEdge(1,11);

        Iterator iterator = testSubject.iteratorBFS(1);

        Assertions.assertEquals(1,iterator.next());
        Assertions.assertEquals(2,iterator.next());
        Assertions.assertEquals(11,iterator.next());

        for(int i=3;iterator.hasNext();i++){
            Assertions.assertEquals(i,iterator.next());
        }
    }
}
