package Graph;

import Exceptions.EmptyCollectionException;
import Exceptions.InvalidIndexException;
import Graphs.Graph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class isConnectedTest {
    /**
     * Testing Class
     */
    private Graph<Integer> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new Graph<>();
    }

    /**
     * Id: isConnectedTest02
     * Pré-condição: Coleção vazia
     * Input: N/A
     * Pós-Condição: EmptyCollectionException
     * Tipo: Causa-Efeito
     */
    @Test
    public void isConnectedTest02() throws EmptyCollectionException {
        Assertions.assertEquals(false,testSubject.isConnected());
    }

    /**
     * Id: isConnectedTest03
     * Pré-condição: Grafo conexo
     * Input: N/A
     * Pós-Condição: True
     * Tipo: Causa-Efeito
     */
    @Test
    public void isConnectedTest03() throws EmptyCollectionException, InvalidIndexException {
        testSubject.addVertex(1);
        testSubject.addVertex(2);
        testSubject.addEdge(1,2);

        Assertions.assertEquals(true,testSubject.isConnected());
    }

    /**
     * Id: isConnectedTest04
     * Pré-condição: Grafo conexo
     * Input: N/A
     * Pós-Condição: False
     * Tipo: Causa-Efeito
     */
    @Test
    public void isConnectedTest04() throws EmptyCollectionException, InvalidIndexException {
        testSubject.addVertex(1);
        testSubject.addVertex(2);

        Assertions.assertEquals(false,testSubject.isConnected());
    }
}
