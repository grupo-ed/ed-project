package Graph;

import Exceptions.EmptyCollectionException;
import Exceptions.InvalidIndexException;
import Graphs.Graph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

public class removeEdgeTest {
    /**
     * Testing Class
     */
    private Graph<Integer> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new Graph<>();
    }

    /**
     * Id: removeEdgeTest03
     * Pré-condição: Coleção vazia
     * Input: N/A
     * Pós-Condição: InvalidIndexException
     * Tipo: Causa-Efeito
     */
    @Test
    public void removeEdgeTest03(){
        Assertions.assertThrows(InvalidIndexException.class, ()->testSubject.removeEdge(1,2));
    }

    /**
     * Id: removeEdgeTest04
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: InvalidIndexException
     * Tipo: Causa-Efeito
     */
    @Test
    public void removeEdgeTest04(){
        for(int i=3;i<=12;i++){
            testSubject.addVertex(i);
        }

        Assertions.assertThrows(InvalidIndexException.class, ()->testSubject.removeEdge(1,2));
    }

    /**
     * Id: removeEdgeTest05
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: InvalidIndexException
     * Tipo: Causa-Efeito
     */
    @Test
    public void removeEdgeTest05() throws InvalidIndexException, EmptyCollectionException {
        testSubject.addVertex(1);
        for(int i=2;i<=10;i++){
            testSubject.addVertex(i);
            testSubject.addEdge(i-1,i);
        }

        Assertions.assertDoesNotThrow(()->testSubject.removeEdge(1,2));
        Iterator iterator = testSubject.iteratorBFS(1);
        iterator.next();

        Assertions.assertEquals(false,iterator.hasNext());
    }
}
