package Graph;

import Exceptions.EmptyCollectionException;
import Exceptions.InvalidIndexException;
import Graphs.Graph;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.management.InstanceNotFoundException;
import java.util.Iterator;

public class iteratorShortestPath {
    /**
     * Testing Class
     */
    private Graph<Integer> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new Graph<>();
    }

    /**
     * Id: iteratorShortestPathTest03
     * Pré-condição: Coleção vazia
     * Input: N/A
     * Pós-Condição: Iterador vazio
     * Tipo: Causa-Efeito
     */
    @Test
    public void iteratorShortestPathTest03() throws EmptyCollectionException, InstanceNotFoundException {
        Assertions.assertEquals(false, testSubject.iteratorShortestPath(1,2).hasNext());
    }

    /**
     * Id: iteratorShortestPathTest04
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: Iterador vazio
     * Tipo: Causa-Efeito
     */
    @Test
    public void iteratorShortestPathTest04() throws EmptyCollectionException, InstanceNotFoundException {
        for(int i=2;i<=11;i++){
            testSubject.addVertex(i);
        }

        Assertions.assertEquals(false, testSubject.iteratorShortestPath(1,2).hasNext());
    }

    /**
     * Id: iteratorShortestPathTest05
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: Iterador BFS
     * Tipo: Causa-Efeito
     */
    @Test
    public void iteratorShortestPathTest05() throws EmptyCollectionException, InvalidIndexException, InstanceNotFoundException {
        testSubject.addVertex(1);
        for(int i=2;i<=10;i++){
            testSubject.addVertex(i);
            testSubject.addEdge(i-1,i);
        }

        testSubject.addVertex(11);
        testSubject.addEdge(1,11);
        testSubject.addEdge(11,10);

        Iterator iterator = testSubject.iteratorShortestPath(1,10);

        Assertions.assertEquals(1,iterator.next());
        Assertions.assertEquals(11,iterator.next());
        Assertions.assertEquals(10,iterator.next());
        Assertions.assertEquals(false,iterator.hasNext());
    }

}
