package LinkedStack;

import Exceptions.EmptyCollectionException;
import Stacks.LinkedStack;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class popTest {

    /**
     * Testing Class
     */
    private LinkedStack<Integer> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp() {
        testSubject = new LinkedStack<>();
    }

    /**
     * Id: popTest03
     * Pré-condição: Coleção vazia
     * Input: N/A
     * Pós-Condição: EmptyCollectionException
     * Tipo: Causa-Efeito
     */
    @Test
    public void popTest03() {
        Assertions.assertThrows(EmptyCollectionException.class, ()->testSubject.pop());
    }

    /**
     * Id: popTest04
     * Pré-condição: Coleção com elementos
     * Input: N/A
     * Pós-Condição: Elemento removido e retornado
     * Tipo: Causa-Efeito
     */
    @Test
    public void popTest04() throws EmptyCollectionException {
        for(int i=0;i<4;i++){
            testSubject.push(i);
        }
        Assertions.assertDoesNotThrow(()->testSubject.pop());
        Assertions.assertNotEquals(3,testSubject.peek());
    }
}
