package LinkedStack;

import Exceptions.EmptyCollectionException;
import Stacks.LinkedStack;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class pushTest {

    /**
     * Testing Class
     */
    private LinkedStack<Integer> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp() {
        testSubject = new LinkedStack<>();
    }

    /**
     * Id: pushTest03
     * Pré-condição: Coleção vazia
     * Input: N/A
     * Pós-Condição: Elemento adicionado
     * Tipo: Causa-Efeito
     */
    @Test
    public void pushTest03() throws EmptyCollectionException {
        Assertions.assertDoesNotThrow(()->testSubject.push(1));
        Assertions.assertEquals(1,testSubject.peek());
    }

    /**
     * Id: pushTest04
     * Pré-condição: Coleção com elementos
     * Input: N/A
     * Pós-Condição: Elemento adicionado
     * Tipo: Causa-Efeito
     */
    @Test
    public void pushTest04() throws EmptyCollectionException {
        for(int i=0;i<4;i++){
            testSubject.push(i);
        }
        Assertions.assertDoesNotThrow(()->testSubject.push(1));
        Assertions.assertEquals(1,testSubject.peek());
    }
}
