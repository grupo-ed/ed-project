package Heap;

import Tree.Heap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class addElementTest {

    /**
     * Testing Class
     */
    private Heap<String> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new Heap<>();
    }

    /**
     * Id: addElementTest01
     * Pré-condição: Heap instanciada
     * Input: "2"
     * Pós-Condição: N/A
     * Tipo: ECP
     */
    @Test
    public void addElementTest01(){
        Assertions.assertDoesNotThrow(()->testSubject.addElement("2"));
    }

    /**
     * Id: addElementTest02
     * Pré-condição: Heap instanciada
     * Input: null
     * Pós-Condição: does not throw
     * Tipo: ECP/BVA
     */
    @Test
    public void addElementTest02(){
        Assertions.assertDoesNotThrow(()->testSubject.addElement(null));
    }
}
