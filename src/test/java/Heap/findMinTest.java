package Heap;

import Exceptions.EmptyCollectionException;
import Tree.Heap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.management.InstanceNotFoundException;

public class findMinTest {

    /**
     * Testing Class
     */
    private Heap<String> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new Heap<>();
    }

    /**
     * Id: findMinTest01
     * Pré-condição: Heap vazia
     * Input: N/A
     * Pós-Condição: EmptyCollectionException
     * Tipo: Causa-Efeito
     */
    @Test
    public void findMinTest01(){
        Assertions.assertThrows(EmptyCollectionException.class ,()->testSubject.removeMin());
    }

    /**
     * Id: findMinTest02
     * Pré-condição: Heap com elementos "2", "3"
     * Input: N/A
     * Pós-Condição: "2"
     * Tipo: Causa-Efeito
     */
    @Test
    public void findMinTest02() throws EmptyCollectionException, InstanceNotFoundException {
        testSubject.addElement("2");
        testSubject.addElement("3");
        Assertions.assertEquals("2" ,testSubject.findMin());

        //para verificar que foi nao removido
        Assertions.assertEquals("2" ,testSubject.findMin());
    }
}
