package ArrayUnorderedList;

import Exceptions.EmptyCollectionException;
import Exceptions.NotFoundException;
import Lists.ArrayUnorderedList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

public class addAfterTest {

    /**
     * Testing Class
     */
    private ArrayUnorderedList<Integer> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp() {
        testSubject = new ArrayUnorderedList<>(10);
    }

    /**
     * Id: addAfterTest03
     * Pré-condição: Coleção vazia
     * Input: N/A
     * Pós-Condição: Elemento adicionado
     * Tipo: Causa-Efeito
     */
    @Test
    public void addAfterTest03() throws EmptyCollectionException {
        Assertions.assertThrows(NotFoundException.class, ()->testSubject.addAfter(1,2));
        Assertions.assertEquals(0,testSubject.size());
    }

    /**
     * Id: addAfterTest04
     * Pré-condição: Coleção com elementos
     * Input: N/A
     * Pós-Condição: Elemento adicionado
     * Tipo: Causa-Efeito
     */
    @Test
    public void addAfterTest04() throws EmptyCollectionException {
        for(int i=0;i<10;i++){
            testSubject.addToFront(i);
        }

        Assertions.assertThrows(NotFoundException.class, ()->testSubject.addAfter(1,10));
        Assertions.assertEquals(10,testSubject.size());
    }

    /**
     * Id: addAfterTest05
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: Elemento adicionado
     * Tipo: Causa-Efeito
     */
    @Test
    public void addAfterTest05() throws EmptyCollectionException {
        for(int i=2;i<13;i++){
            testSubject.addToFront(i);
        }
        Assertions.assertDoesNotThrow(()->testSubject.addAfter(1,2));

        Iterator iterator = testSubject.iterator();

        while (iterator.hasNext()){
            if(iterator.next().equals(2)){
                Assertions.assertEquals(1,iterator.next());
            } else {
                Assertions.assertNotEquals(1,iterator.next());
            }
        }
    }
}
