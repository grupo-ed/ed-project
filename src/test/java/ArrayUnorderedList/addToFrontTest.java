package ArrayUnorderedList;

import Exceptions.EmptyCollectionException;
import Lists.ArrayUnorderedList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class addToFrontTest {

    /**
     * Testing Class
     */
    private ArrayUnorderedList<Integer> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp() {
        testSubject = new ArrayUnorderedList<>(10);
    }

    /**
     * Id: addToFrontTest03
     * Pré-condição: Coleção vazia
     * Input: N/A
     * Pós-Condição: Elemento adicionado
     * Tipo: Causa-Efeito
     */
    @Test
    public void addToFrontTest03() throws EmptyCollectionException {
        Assertions.assertDoesNotThrow(()->testSubject.addToFront(1));
        Assertions.assertEquals(1,testSubject.first());
        Assertions.assertEquals(1,testSubject.size());
    }

    /**
     * Id: addToFrontTest04
     * Pré-condição: Coleção com elementos
     * Input: N/A
     * Pós-Condição: Elemento adicionado
     * Tipo: Causa-Efeito
     */
    @Test
    public void addToFrontTest04() throws EmptyCollectionException {
        for(int i=0;i<4;i++){
            testSubject.addToFront(i);
        }
        Assertions.assertDoesNotThrow(()->testSubject.addToFront(1));
        Assertions.assertEquals(1,testSubject.first());
        Assertions.assertEquals(5,testSubject.size());
    }

    /**
     * Id: addToFrontTest05
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: Elemento adicionado
     * Tipo: Causa-Efeito
     */
    @Test
    public void addToFrontTest05() throws EmptyCollectionException {
        for(int i=0;i<10;i++){
            testSubject.addToFront(i);
        }
        Assertions.assertDoesNotThrow(()->testSubject.addToFront(1));
        Assertions.assertEquals(1,testSubject.first());
        Assertions.assertEquals(11,testSubject.size());
    }
}

