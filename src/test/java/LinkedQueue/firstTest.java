package LinkedQueue;

import Exceptions.EmptyCollectionException;
import Queues.LinkedQueue;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class firstTest {
    /**
     * Testing Class
     */
    private LinkedQueue<String> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new LinkedQueue<>();
    }

    /**
     * Id: firstTest01
     * Pré-condição: LinkedQueue instanciada sem elementos
     * Input: N/A
     * Pós-Condição: EmptyCollectionException
     * Tipo: Causa-Efeito
     */
    @Test
    public void enqueueTest01(){
        Assertions.assertThrows(EmptyCollectionException.class , ()->testSubject.first());
    }

    /**
     * Id: firstTest02
     * Pré-condição: LinkedQueue instanciada com elemento "1","2"
     * Input: N/A
     * Pós-Condição: "1"
     * Tipo: Causa-Efeito
     */
    @Test
    public void enqueueTest02() throws EmptyCollectionException {
        testSubject.enqueue("1");
        testSubject.enqueue("2");
        Assertions.assertEquals("1",testSubject.first());
    }
}
