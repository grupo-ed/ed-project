package LinkedQueue;

import Queues.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class enqueueTest {

    /**
     * Testing Class
     */
    private LinkedQueue<String> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new LinkedQueue<>();
    }

    /**
     * Id: enqueueTest01
     * Pré-condição: LinkedQueue instanciada
     * Input: "String"
     * Pós-Condição: N/A
     * Tipo: ECP
     */
    @Test
    public void enqueueTest01(){
        Assertions.assertDoesNotThrow(()->testSubject.enqueue("string"));
    }

    /**
     * Id: enqueueTest02
     * Pré-condição: LinkedQueue instanciada
     * Input: null
     * Pós-Condição: NullPointerException
     * Tipo: ECP/BVA
     */
    @Test
    public void enqueueTest02(){
        Assertions.assertThrows(NullPointerException.class,()->testSubject.enqueue(null));
    }
}
