package LinkedQueue;

import Exceptions.EmptyCollectionException;
import Queues.LinkedQueue;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class dequeueTest {
    /**
     * Testing Class
     */
    private LinkedQueue<String> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new LinkedQueue<>();
    }

    /**
     * Id: dequeueTest01
     * Pré-condição: LinkedQueue instanciada com elemento "1"
     * Input: N/A
     * Pós-Condição: "1"
     * Tipo: Causa-Efeito
     */
    @Test
    public void enqueueTest01() throws EmptyCollectionException {
        testSubject.enqueue("1");
        Assertions.assertEquals("1",testSubject.dequeue());
    }

    /**
     * Id: dequeueTest02
     * Pré-condição: LinkedQueue instanciada sem elemento
     * Input: N/A
     * Pós-Condição: EmptyCollectionException
     * Tipo: Causa-Efeito
     */
    @Test
    public void enqueueTest02(){
        Assertions.assertThrows(EmptyCollectionException.class , ()->testSubject.dequeue());
    }
}
