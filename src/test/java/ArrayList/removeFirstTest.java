package ArrayList;

import Exceptions.EmptyCollectionException;
import Lists.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class removeFirstTest {

    /**
     * Testing Class
     */
    private ArrayUnorderedList<String> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new ArrayUnorderedList<>();
    }

    /**
     * Id: removeFirstTest01
     * Pré-condição: ArrayList sem elementos
     * Input: N/A
     * Pós-Condição: EmptyCollectionException
     * Tipo: Causa-Efeito
     */
    @Test
    public void removeFirstTest01(){
        Assertions.assertThrows(EmptyCollectionException.class, ()->testSubject.removeFirst());
    }

    /**
     * Id: removeFirstTest02
     * Pré-condição: ArrayList com elementos Front , 1 , 2
     * Input: N/A
     * Pós-Condição: "Front"
     * Tipo: Causa-Efeito
     */
    @Test
    public void removeFirstTest02() throws EmptyCollectionException {
        testSubject.addToFront("1");
        testSubject.addToRear("2");
        testSubject.addToFront("Front");
        Assertions.assertEquals("Front", testSubject.removeFirst());
    }

}
