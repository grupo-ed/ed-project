package ArrayList;

import Exceptions.EmptyCollectionException;
import Exceptions.NotFoundException;
import Lists.ArrayUnorderedList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class removeTest {
    /**
     * Testing Class
     */
    private ArrayUnorderedList<String> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new ArrayUnorderedList<>();
    }

    /**
     * Id: removeTest01
     * Pré-condição: ArrayList sem elementos
     * Input: N/A
     * Pós-Condição: EmptyCollectionException
     * Tipo: Causa-Efeito
     */
    @Test
    public void removeTest01(){
        Assertions.assertThrows(EmptyCollectionException.class, ()->testSubject.remove("remove"));
    }

    /**
     * Id: removeTest02
     * Pré-condição: ArrayList com elementos: Front,1,2
     * Input: "2"
     * Pós-Condição: "2"
     * Tipo: Causa-Efeito
     */
    @Test
    public void removeTest02() throws EmptyCollectionException, NotFoundException {
        testSubject.addToFront("1");
        testSubject.addToRear("2");
        testSubject.addToFront("Front");
        Assertions.assertEquals("2", testSubject.remove("2"));
    }

    /**
     * Id: removeTest03
     * Pré-condição: ArrayList com elementos: Front , 1 , 2
     * Input: "3"
     * Pós-Condição: NotFoundException
     * Tipo: Causa-Efeito
     */
    @Test
    public void removeTest03(){
        testSubject.addToFront("1");
        testSubject.addToRear("2");
        testSubject.addToFront("Front");
        Assertions.assertThrows(NotFoundException.class, ()->testSubject.remove("3"));
    }

    /**
     * Id: removeTest06
     * Pré-condição: ArrayList com elementos: Front , 1 , 2
     * Input: null
     * Pós-Condição: NullPointerException
     * Tipo: ECP/BVA
     */
    @Test
    public void removeTest06(){
        testSubject.addToFront("1");
        testSubject.addToRear("2");
        testSubject.addToFront("Front");
        Assertions.assertThrows(NullPointerException.class, ()->testSubject.remove(null));
    }
}
