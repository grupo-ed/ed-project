package ArrayList;

import Lists.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class ArrayListTest {

    /**
     * Id: ArrayListTest01
     * Pré-condição: N/A
     * Input: 1
     * Pós-Condição: N/A
     * Tipo: ECP/ BVA
     */
    @Test
    public void ArrayListTest01(){
        Assertions.assertDoesNotThrow(()-> new ArrayList<Integer>(1));
    }

    /**
     * Id: ArrayListTest04
     * Pré-condição: N/A
     * Input: 0
     * Pós-Condição: N/A
     * Tipo: BVA
     */
    @Test
    public void ArrayListTest04(){
        Assertions.assertDoesNotThrow(()-> new ArrayList<Integer>(0));
    }

    /**
     * Id: ArrayListTest05
     * Pré-condição: N/A
     * Input: -1
     * Pós-Condição: NegativeArraySizeException
     * Tipo: BVA
     */
    @Test
    public void ArrayListTest05(){
        Assertions.assertThrows(NegativeArraySizeException.class , ()-> new ArrayList<Integer>(-1));
    }
}
