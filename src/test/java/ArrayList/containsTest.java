package ArrayList;

import Lists.ArrayUnorderedList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class containsTest {

    /**
     * Testing Class
     */
    private ArrayUnorderedList<String> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new ArrayUnorderedList<>();
    }

    /**
     * Id: containsTest01
     * Pré-condição: ArrayList sem elementos
     * Input: "2"
     * Pós-Condição: false
     * Tipo: Causa-Efeito
     */
    @Test
    public void containsTest01(){
        Assertions.assertEquals(false,testSubject.contains("2"));
    }

    /**
     * Id: containsTest02
     * Pré-condição: ArrayList com elementos Front , 1 , 2
     * Input: "2"
     * Pós-Condição: true
     * Tipo: Causa-Efeito
     */
    @Test
    public void containsTest02() {
        testSubject.addToFront("1");
        testSubject.addToRear("2");
        testSubject.addToFront("Front");
        Assertions.assertEquals(true,testSubject.contains("2"));
    }

    /**
     * Id: containsTest03
     * Pré-condição: ArrayList com elementos Front , 1 , 2
     * Input: "3"
     * Pós-Condição: false
     * Tipo: Causa-Efeito
     */
    @Test
    public void containsTest03() {
        testSubject.addToFront("1");
        testSubject.addToRear("2");
        testSubject.addToFront("Front");
        Assertions.assertEquals(false,testSubject.contains("3"));
    }

    /**
     * Id: containsTest06
     * Pré-condição: ArrayList com elementos Front , 1 , 2
     * Input: null
     * Pós-Condição: NullPointerException
     * Tipo: ECP/BVA
     */
    @Test
    public void containsTest06() {
        testSubject.addToFront("1");
        testSubject.addToRear("2");
        testSubject.addToFront("Front");
        Assertions.assertThrows(NullPointerException.class,()->testSubject.contains(null));
    }
}
