package ArrayList;

import Exceptions.EmptyCollectionException;
import Lists.ArrayUnorderedList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class lastTest {
    /**
     * Testing Class
     */
    private ArrayUnorderedList<String> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new ArrayUnorderedList<>();
    }

    /**
     * Id: lastTest01
     * Pré-condição: ArrayList sem elementos
     * Input: N/A
     * Pós-Condição: EmptyCollectionException
     * Tipo: Causa-Efeito
     */
    @Test
    public void lastTest01(){
        Assertions.assertThrows(EmptyCollectionException.class, ()->testSubject.last());
    }

    /**
     * Id: lastTest02
     * Pré-condição: ArrayList com elementos Front , 1 , 2
     * Input: N/A
     * Pós-Condição: "2"
     * Tipo: Causa-Efeito
     */
    @Test
    public void lastTest02() throws EmptyCollectionException {
        testSubject.addToFront("1");
        testSubject.addToRear("2");
        testSubject.addToFront("Front");
        Assertions.assertEquals("2", testSubject.last());
    }
}
