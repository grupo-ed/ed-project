package Network;

import Exceptions.EmptyCollectionException;
import Exceptions.InvalidIndexException;
import Graphs.Network;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.management.InstanceNotFoundException;
import java.util.Iterator;

public class iteratorShortestPathWeightTest {
    /**
     * Testing Class
     */
    private Network<Integer> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new Network<>();
    }

    /**
     * Id: iteratorShortestPathWeightTest03
     * Pré-condição: Coleção vazia
     * Input: N/A
     * Pós-Condição: EmptyCollectionException.
     * Tipo: Causa-Efeito
     */
    @Test
    public void iteratorShortestPathWeightTest03() {
        Assertions.assertThrows(EmptyCollectionException.class,()-> testSubject.iteratorShortestPath(1,2).hasNext());
    }

    /**
     * Id: iteratorShortestPathWeightTest04
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: false
     * Tipo: Causa-Efeito
     */
    @Test
    public void iteratorShortestPathWeightTest04() throws EmptyCollectionException, InstanceNotFoundException {
        for(int i=2;i<=11;i++){
            testSubject.addVertex(i);
        }

        Assertions.assertEquals(false, testSubject.iteratorShortestPath(1,2).hasNext());
    }

    /**
     * Id: iteratorShortestPathWeightTest05
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: Iterador BFS
     * Tipo: Causa-Efeito
     */
    @Test
    public void iteratorShortestPathWeightTest05() throws EmptyCollectionException, InvalidIndexException, InstanceNotFoundException {
        testSubject.addVertex(1);

        for(int i=2;i<=10;i++){
            testSubject.addVertex(i);
            testSubject.addEdge(i-1, i ,1);
        }

        testSubject.addVertex(11);
        testSubject.addEdge(1,11,10);
        testSubject.addEdge(11,10,15);

        Iterator iterator = testSubject.iteratorShortestPath(1,10);
        Object[] vertices = testSubject.getVertices();

        for(int i=1;iterator.hasNext();i++){
            Assertions.assertEquals(i,vertices[(int) iterator.next()]);
        }
    }

}
