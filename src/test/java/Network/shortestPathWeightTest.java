package Network;

import Exceptions.EmptyCollectionException;
import Exceptions.InvalidIndexException;
import Graphs.Network;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class shortestPathWeightTest {
    /**
     * Testing Class
     */
    private Network<Integer> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new Network<>();
    }

    /**
     * Id: shortestPathWeightTest03
     * Pré-condição: Coleção vazia
     * Input: N/A
     * Pós-Condição: POSITIVE_INFINITY
     * Tipo: Causa-Efeito
     */
    @Test
    public void shortestPathWeightTest03() {
        Assertions.assertEquals(Double.POSITIVE_INFINITY, testSubject.shortestPathWeight(1,2));
    }

    /**
     * Id: shortestPathWeightTest04
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: POSITIVE_INFINITY
     * Tipo: Causa-Efeito
     */
    @Test
    public void shortestPathWeightTest04() {
        for(int i=2;i<=11;i++){
            testSubject.addVertex(i);
        }

        Assertions.assertEquals(Double.POSITIVE_INFINITY, testSubject.shortestPathWeight(1,2));
    }

    /**
     * Id: shortestPathWeightTest05
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: 9
     * Tipo: Causa-Efeito
     */
    @Test
    public void shortestPathWeightTest05() throws InvalidIndexException {
        testSubject.addVertex(1);
        for(int i=2;i<=10;i++){
            testSubject.addVertex(i);
            testSubject.addEdge(i-1,i,1);
        }

        testSubject.addVertex(11);
        testSubject.addEdge(1,11,10);
        testSubject.addEdge(11,10,15);

         Assertions.assertEquals(9, testSubject.shortestPathWeight(1,10));
    }
}
