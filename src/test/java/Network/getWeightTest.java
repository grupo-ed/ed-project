package Network;

import Exceptions.EmptyCollectionException;
import Exceptions.InvalidIndexException;
import Graphs.Network;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class getWeightTest {
    /**
     * Testing Class
     */
    private Network<Integer> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new Network<>();
    }

    /**
     * Id: gethWeightTest03
     * Pré-condição: Coleção vazia
     * Input: N/A
     * Pós-Condição: 0
     * Tipo: Causa-Efeito
     */
    @Test
    public void getWeightTest03() {
        Assertions.assertThrows(InvalidIndexException.class,()-> testSubject.getWeight(1,2));
    }

    /**
     * Id: getWeightTest04
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: 0
     * Tipo: Causa-Efeito
     */
    @Test
    public void getWeightTest04() {
        for(int i=2;i<=11;i++){
            testSubject.addVertex(i);
        }

        Assertions.assertThrows(InvalidIndexException.class,()-> testSubject.getWeight(1,2));
    }

    /**
     * Id: getWeightTest05
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: 9
     * Tipo: Causa-Efeito
     */
    @Test
    public void getWeightTest05() throws InvalidIndexException {
        testSubject.addVertex(1);
        for(int i=2;i<=10;i++){
            testSubject.addVertex(i);
            testSubject.addEdge(i-1,i,1);
        }

        testSubject.addVertex(11);
        testSubject.addEdge(1,11,10);
        testSubject.addEdge(11,10,10);

        Assertions.assertEquals(10, testSubject.getWeight(1,11));
    }

    /**
     * Id: getWeightTest06
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: 9
     * Tipo: Causa-Efeito
     */
    @Test
    public void getWeightTest06() throws InvalidIndexException {
        testSubject.addVertex(1);
        for(int i=2;i<=10;i++){
            testSubject.addVertex(i);
            testSubject.addEdge(i-1,i,1);
        }

        testSubject.addVertex(11);
        testSubject.addEdge(1,11,10);
        testSubject.addEdge(11,10,10);

        Assertions.assertEquals(Double.POSITIVE_INFINITY, testSubject.getWeight(1,10));
    }
}
