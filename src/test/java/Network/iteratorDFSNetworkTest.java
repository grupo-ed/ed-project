package Network;

import Exceptions.EmptyCollectionException;
import Exceptions.InvalidIndexException;
import Graphs.Network;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

public class iteratorDFSNetworkTest {
    /**
     * Testing Class
     */
    private Network<Integer> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new Network<>();
    }

    /**
     * Id: iteratorDFSNetworkTest03
     * Pré-condição: Coleção vazia
     * Input: N/A
     * Pós-Condição: Iterador vazio
     * Tipo: Causa-Efeito
     */
    @Test
    public void iteratorDFSNetworkTest03() throws EmptyCollectionException {
        Assertions.assertEquals(false, testSubject.iteratorDFS(1).hasNext());
    }

    /**
     * Id: iteratorDFSNetworkTest04
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: Iterador vazio
     * Tipo: Causa-Efeito
     */
    @Test
    public void iteratorDFSNetworkTest04() throws EmptyCollectionException {
        for(int i=2;i<=11;i++){
            testSubject.addVertex(i);
        }

        Assertions.assertEquals(false, testSubject.iteratorDFS(1).hasNext());
    }

    /**
     * Id: iteratorDFSNetworkTest05
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: Iterador BFS
     * Tipo: Causa-Efeito
     */
    @Test
    public void iteratorDFSNetworkTest05() throws EmptyCollectionException, InvalidIndexException {
        testSubject.addVertex(1);
        for(int i=2;i<=10;i++){
            testSubject.addVertex(i);
            testSubject.addEdge(i-1,i, 3);
        }

        testSubject.addVertex(11);
        testSubject.addEdge(1,11, 3);

        Iterator iterator = testSubject.iteratorDFS(1);

        Assertions.assertEquals(1,iterator.next());
        for(int i=2;i<=10;i++){
            Assertions.assertEquals(i,iterator.next());
        }
        Assertions.assertEquals(11,iterator.next());
        Assertions.assertEquals(false,iterator.hasNext());
    }
}
