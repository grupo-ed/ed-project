package Network;

import Exceptions.EmptyCollectionException;
import Exceptions.InvalidIndexException;
import Graphs.Network;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Iterator;

public class addEdgeNetworkTest {
    /**
     * Testing Class
     */
    private Network<Integer> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp(){
        testSubject = new Network<>();
    }

    /**
     * Id: addEdgeNetworkTest04
     * Pré-condição: Coleção vazia
     * Input: N/A
     * Pós-Condição: InvalidIndexException
     * Tipo: Causa-Efeito
     */
    @Test
    public void addEdgeNetworkTest04(){
        Assertions.assertThrows(InvalidIndexException.class, ()->testSubject.addEdge(1,2));
    }

    /**
     * Id: addEdgeNetworkTest05
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: InvalidIndexException
     * Tipo: Causa-Efeito
     */
    @Test
    public void addEdgeNetworkTest05(){
        for(int i=4;i<14;i++){
            testSubject.addVertex(i);
        }

        Assertions.assertThrows(InvalidIndexException.class, ()->testSubject.addEdge(1,2));
    }

    /**
     * Id: addEdgeNetworkTest06
     * Pré-condição: Coleção cheia
     * Input: N/A
     * Pós-Condição: Aresta adicionada
     * Tipo: Causa-Efeito
     */
    @Test
    public void addEdgeNetworkTest06() throws EmptyCollectionException {
        for(int i=1;i<10;i++){
            testSubject.addVertex(i);
        }

        Assertions.assertDoesNotThrow(()->testSubject.addEdge(1,2,6));

        Iterator iterator = testSubject.iteratorBFS(1);

        Assertions.assertEquals(1, iterator.next());
        Assertions.assertEquals(2, iterator.next());
        Assertions.assertEquals(false, iterator.hasNext());
    }
}
