package Map;

import Exceptions.InvalidIndexException;
import Exceptions.InvalidMapException;
import MapManagement.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class possiblePaths {
    /**
     * Testing Class
     */
    private Map<String> testSubject;

    /**
     * Initialization of testing class
     */
    @BeforeEach
    public void setUp() throws InvalidMapException {
        testSubject = new Map<String>("test",1);
    }

    /**
     * Id: possiblePathsTest01
     * Pré-condição: Map sem vertices
     * Input: entrada
     * Pós-Condição: InvalidIndexException
     * Tipo: Causa-Efeito
     */
    @Test
    public void possiblePathsTest01(){
        Assertions.assertThrows(InvalidIndexException.class,()->testSubject.possiblePaths("entrada"));
    }

    /**
     * Id: possiblePathsTest02
     * Pré-condição: Map com vertices
     * Input: entrada
     * Pós-Condição: saida
     * Tipo: Causa-Efeito
     */
    @Test
    public void possiblePathsTest02() throws InvalidIndexException {
        testSubject.addVertex("entrada");
        testSubject.addVertex("saida");
        testSubject.addEdge("entrada","saida",10);

        Assertions.assertEquals("saida",testSubject.possiblePaths("entrada").next());
    }

    /**
     * Id: possiblePathsTest03
     * Pré-condição: Map com vertices
     * Input: ""sala""
     * Pós-Condição: InvalidIndexException
     * Tipo: Causa-Efeito
     */
    @Test
    public void possiblePathsTest03() throws InvalidIndexException {
        testSubject.addVertex("entrada");
        testSubject.addVertex("saida");
        testSubject.addEdge("entrada","saida");

        Assertions.assertThrows(InvalidIndexException.class,()->testSubject.possiblePaths("sala"));
    }
}
