package Map;

import Exceptions.InvalidMapException;
import MapManagement.Map;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Maptest {
    /**
     * Id: mapTest01
     * Pré-condição: N/A
     * Input: name,0
     * Pós-Condição: InvalidMapException
     * Tipo: Causa-Efeito
     */
    @Test
    public void mapTest01(){
        Assertions.assertThrows(InvalidMapException.class, ()->new Map<>("name",0));
    }

    /**
     * Id: mapTest02
     * Pré-condição: N/A
     * Input: name,1
     * Pós-Condição: Mapa criado
     * Tipo: Causa-Efeito
     */
    @Test
    public void mapTest02(){
        Assertions.assertDoesNotThrow(()->new Map<>("name",1));
    }

    /**
     * Id: mapTest03
     * Pré-condição: N/A
     * Input: name,-1
     * Pós-Condição: InvalidMapException
     * Tipo: Causa-Efeito
     */
    @Test
    public void mapTest03(){
        Assertions.assertThrows(InvalidMapException.class, ()->new Map<>("name",-1));
    }

    /**
     * Id: mapTest05
     * Pré-condição: N/A
     * Input:  ,1
     * Pós-Condição: InvalidMapException
     * Tipo: Causa-Efeito
     */
    @Test
    public void mapTest05(){
        Assertions.assertThrows(InvalidMapException.class, ()->new Map<>("",-1));
    }
}

