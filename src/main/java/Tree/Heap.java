package Tree;
import Exceptions.*;

/**
 * Represents an implementation of a heap.
 * @param <T> Specify by the user
 */
public class Heap <T> extends LinkedBinaryTree<T> implements HeapADT<T>{

    private HeapNode<T> lastNodeInserted;

    /**
     * Constructs an empty heap.
     */
    public Heap() {
    }

    /**
     * Constructs with element
     * @param element
     */
    public Heap(T element) {
        super(element);
    }

    /**
     * {@inheritDoc}
     * @param element the element to added to this head
     */
    @Override
    public void addElement(T element) {
        HeapNode<T> newNode = new HeapNode(element);
        if (this.isEmpty()) {
            this.root = newNode;
        } else {
            HeapNode<T> nextParent = this.getNextParentAdd();
            if (nextParent.getLeft() == null) {
                nextParent.setLeft(newNode);
            } else {
                nextParent.setRight(newNode);
            }

            newNode.setParent(nextParent);
        }

        this.lastNodeInserted = newNode;
        ++this.count;
        if (this.count > 1) {
            this.heapifyAdd();
        }

    }

    /**
     * {@inheritDoc}
     * @return Element removed
     * @throws EmptyCollectionException When collection is empty
     */
    @Override
    public T removeMin() throws EmptyCollectionException {
        if (this.isEmpty()) {
            throw new EmptyCollectionException("Empty heap");
        } else {
            T minElement = this.root.getElement();
            if (this.count == 1) {
                this.root = null;
                this.lastNodeInserted = null;
            } else {
                HeapNode<T> nextLast = this.getNewLastNode();
                if (this.lastNodeInserted.getParent().getLeft() == this.lastNodeInserted) {
                    this.lastNodeInserted.getParent().setLeft((BinaryTreeNode)null);
                } else {
                    this.lastNodeInserted.getParent().setRight((BinaryTreeNode)null);
                }

                this.root.setElement(this.lastNodeInserted.getElement());
                this.lastNodeInserted = nextLast;
                this.heapifyRemove();
            }

            --this.count;
            return minElement;
        }
    }

    /**
     * {@inheritDoc }
     * @return the minimum element
     */
    @Override
    public T findMin() {
        return this.root.getElement();
    }

    /**
     * Returns the node that will be the parent of the new node.
     * @return The parent of the new node
     */
    private HeapNode<T> getNextParentAdd() {
        HeapNode result;
        for(result = this.lastNodeInserted; result != this.root && result.getParent().getLeft() != result; result = result.getParent()) {
        }

        if (result != this.root) {
            if (result.getParent().getRight() == null) {
                result = result.getParent();
            } else {
                for(result = (HeapNode)result.getParent().getRight(); result.getLeft() != null; result = (HeapNode)result.getLeft()) {
                }
            }
        } else {
            while(result.getLeft() != null) {
                result = (HeapNode)result.getLeft();
            }
        }

        return result;
    }

    /**
     * Reorders the heap after adding a node
     */
    private void heapifyAdd() {
        HeapNode<T> next = this.lastNodeInserted;

        Object temp;
        for(temp = next.getElement(); next != this.root && ((Comparable)temp).compareTo(next.getParent().getElement()) < 0; next = next.getParent()) {
            next.setElement(next.getParent().getElement());
        }

        next.setElement((T) temp);
    }

    /**
     * Returns the node that will be the new last node after a remove.
     * @return The new Last node
     */
    private HeapNode<T> getNewLastNode() {
        HeapNode result;
        for(result = this.lastNodeInserted; result != this.root && result.getParent().getLeft() == result; result = result.getParent()) {
        }

        if (result != this.root) {
            result = (HeapNode)result.getParent().getLeft();
        }

        while(result.getRight() != null) {
            result = (HeapNode)result.getRight();
        }

        return result;
    }

    /**
     * Reorders the heap after removing the root element.
     */
    private void heapifyRemove() {
        HeapNode<T> node = (HeapNode)this.root;
        HeapNode<T> left = (HeapNode)node.getLeft();
        HeapNode<T> right = (HeapNode)node.getRight();
        HeapNode next;
        if (left == null && right == null) {
            next = null;
        } else if (left == null) {
            next = right;
        } else if (right == null) {
            next = left;
        } else if (((Comparable)left.getElement()).compareTo(right.getElement()) < 0) {
            next = left;
        } else {
            next = right;
        }

        Object temp = node.getElement();

        while(next != null && ((Comparable)next.getElement()).compareTo(temp) < 0) {
            node.setElement((T) next.getElement());
            node = next;
            left = (HeapNode)next.getLeft();
            right = (HeapNode)next.getRight();
            if (left == null && right == null) {
                next = null;
            } else if (left == null) {
                next = right;
            } else if (right == null) {
                next = left;
            } else if (((Comparable)left.getElement()).compareTo(right.getElement()) < 0) {
                next = left;
            } else {
                next = right;
            }
        }

        node.setElement((T) temp);
    }

    /**
     * Remove all elements of Heap
     */
    public void removeAllElements(){
        this.root = null;
        this.lastNodeInserted = null;
        this.count = 0;
    }
}
