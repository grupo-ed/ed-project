package Tree;

/**
 * Creates a binary tree node with a parent pointer for
 * use in heaps.
 * @param <T> Specify by the user
 */
public class HeapNode<T> extends BinaryTreeNode<T>{

    /**
     * The node right above
     */
    private HeapNode<T> parent;

    /**
     * Creates a new heap node with the specified data
     * @param obj Object being added
     */
    HeapNode (T obj)
    {
        super(obj);
        parent = null;
    }

    /**
     * Get parent
     * @return parent
     */
    public HeapNode<T> getParent() {
        return parent;
    }

    /**
     * Set Parent
     * @param parent setting parent
     */
    public void setParent(HeapNode<T> parent) {
        this.parent = parent;
    }
}
