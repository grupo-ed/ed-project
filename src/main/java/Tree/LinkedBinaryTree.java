package Tree;

import Exceptions.*;
import Lists.*;

import java.util.Iterator;

/**
 * Implements the BinaryTreeADT interface
 * @param <T> Specify by the user
 */
public class LinkedBinaryTree<T> implements BinaryTreeADT<T> {
    protected int count;
    protected BinaryTreeNode<T> root;

    /**
     * Creates an empty binary tree.
     */
    public LinkedBinaryTree()
    {
        count = 0;
        root = null;
    }

    /**
     * Creates a binary tree with the specified element as its root.
     * @param element element being added
     */
    public LinkedBinaryTree (T element) {
        if(element==null){
            throw new NullPointerException();
        }
        count = 1;
        root = new BinaryTreeNode<T> (element);
    }

    /**
     * Constructs a binary tree from the two specified binary trees.
     * @param element element
     * @param leftSubtree left sub tree
     * @param rightSubtree right sub tree
     */
    public LinkedBinaryTree (T element, LinkedBinaryTree<T> leftSubtree,
                             LinkedBinaryTree<T> rightSubtree)
    {
        root = new BinaryTreeNode<T> (element);
        count = 1;

        if (leftSubtree != null)
        {
            count = count + leftSubtree.size();
            root.setLeft(leftSubtree.root);
        }
        else
            root.setLeft(null);

        if (rightSubtree !=null)
        {
            count = count + rightSubtree.size();
            root.setRight(rightSubtree.root);
        }
        else
            root.setRight(null);
    }

    /**
     * {@inheritDoc}
     * @return root
     */
    public T getRoot(){
        return root.getElement();
    }

    /**
     * Returns true if this binary tree is empty and false otherwise.
     * @return if is empty or not
     */
    public boolean isEmpty()
    {
        return (count == 0);
    }

    /**
     * Returns true if this binary tree is empty and false otherwise.
     * @return size
     */
    public int size()
    {
        return count;
    }

    /**
     * Returns true if this tree contains an element that matches the
     * specified target element and false otherwise.
     * @param targetElement element to search
     * @return if contains or not.
     */
    public boolean contains (T targetElement){
        T temp;
        boolean found = false;

        try
        {
            temp = find(targetElement);
            found = true;
        }
        catch (Exception ElementNotFoundException)
        {
            found = false;
        }

        return found;
    }

    /**
     * Returns a reference to the specified target element if it is
     * found in this binary tree.  Throws a NoSuchElementException if
     * the specified target element is not found in the binary tree.
     * @param targetElement element to search
     * @return element
     * @throws NotFoundException If isn't found
     */
    public T find(T targetElement) throws NotFoundException
    {
        if(targetElement==null){
            throw new NullPointerException();
        }
        BinaryTreeNode<T> current = findAgain( targetElement, root );

        if( current == null )
            throw new NotFoundException("binary tree");

        return (current.getElement());
    }

    /**
     * Returns a reference to the specified target element if it is
     *  found in this binary tree.
     * @param targetElement
     * @param next
     * @return
     */
    private BinaryTreeNode<T> findAgain(T targetElement,
                                        BinaryTreeNode<T> next)
    {
        if (next == null)
            return null;

        if (next.getElement().equals(targetElement))
            return next;

        BinaryTreeNode<T> temp = findAgain(targetElement, next.getLeft());

        if (temp == null)
            temp = findAgain(targetElement, next.getRight());

        return temp;
    }

    /**
     * Returns a string representation of this binary tree.
     * @return String
     */
    public String toString()
    {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        preorder (root, tempList);

        return tempList.toString();
    }

    /**
     *  Performs an inorder traversal on this binary tree by calling an
     *  overloaded, recursive inorder method that starts with
     *  the root.
     * @return iterator
     */
    public Iterator<T> iteratorInOrder()
    {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        inorder (root, tempList);

        return tempList.iterator();
    }

    /**
     * Performs a recursive inorder traversal.
     * @param node node
     * @param tempList array
     */
    protected void inorder (BinaryTreeNode<T> node,
                            ArrayUnorderedList<T> tempList)
    {
        if (node != null)
        {
            inorder (node.getLeft(), tempList);
            tempList.addToRear(node.getElement());
            inorder (node.getRight(), tempList);
        }
    }

    /**
     * Performs an preorder traversal on this binary tree by calling
     * an overloaded, recursive preorder method that starts with
     * the root.
     * @return iterator
     */
    public Iterator<T> iteratorPreOrder()
    {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        preorder (root, tempList);

        return tempList.iterator();
    }

    /**
     * Performs a recursive preorder traversal.
     * @param node node
     * @param tempList array
     */
    protected void preorder (BinaryTreeNode<T> node,
                             ArrayUnorderedList<T> tempList)
    {
        if (node != null)
        {
            tempList.addToRear(node.getElement());
            preorder (node.getLeft(), tempList);
            preorder (node.getRight(), tempList);
        }
    }

    /**
     * Performs an postorder traversal on this binary tree by calling
     * an overloaded, recursive postorder method that starts
     * with the root.
     * @return iterator
     */
    public Iterator<T> iteratorPostOrder()
    {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        postorder (root, tempList);

        return tempList.iterator();
    }

    /**
     * Performs a recursive postorder traversal.
     * @param node node
     * @param tempList array
     */
    protected void postorder (BinaryTreeNode<T> node,
                              ArrayUnorderedList<T> tempList)
    {
        if (node != null)
        {
            postorder (node.getLeft(), tempList);
            postorder (node.getRight(), tempList);
            tempList.addToRear(node.getElement());
        }
    }

    /**
     *  Performs a levelorder traversal on this binary tree, using a
     *  templist.
     * @return iterator
     * @throws EmptyCollectionException When collection is empty
     */
    public Iterator<T> iteratorLevelOrder() throws EmptyCollectionException {
        ArrayUnorderedList<BinaryTreeNode<T>> nodes =
                new ArrayUnorderedList<>();
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        BinaryTreeNode<T> current;

        nodes.addToRear (root);

        while (! nodes.isEmpty())
        {
            current = nodes.removeFirst();

            if (current != null)
            {
                tempList.addToRear(current.getElement());
                if (current.getLeft()!=null)
                    nodes.addToRear (current.getLeft());
                if (current.getRight()!=null)
                    nodes.addToRear (current.getRight());
            }
            else
                tempList.addToRear(null);
        }

        return tempList.iterator();
    }
}
