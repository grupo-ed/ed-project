package Tree;

import Exceptions.EmptyCollectionException;

import javax.management.InstanceNotFoundException;

/**
 * Defines the interface to a Heap.
 *
 * @param <T> Specify by the user
 */
public interface HeapADT<T> extends BinaryTreeADT<T>{

    /**
     * Adds the specified object to this heap.
     *
     * @param obj the element to added to this head
     * @throws InstanceNotFoundException When Comparable isn't implemented.
     */
    public void addElement (T obj) throws InstanceNotFoundException;

    /**
     * Removes element with the lowest value from this heap.
     *
     * @return the element with the lowest value from this heap.
     * @throws EmptyCollectionException When collection is empty
     */
    public T removeMin() throws EmptyCollectionException;

    /**
     * Removes element with the lowest value from this heap.
     *
     * @return the element with the lowest value from this heap.
     * @throws EmptyCollectionException When collection is empty
     */
    public T findMin() throws EmptyCollectionException;
}
