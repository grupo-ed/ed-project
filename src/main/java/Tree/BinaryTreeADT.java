package Tree;

import Exceptions.*;

import java.util.Iterator;

/**
 * Defines the interface to a binary tree data structure
 * @param <T> Specified by the user
 */
public interface BinaryTreeADT<T> {

    /**
     * Get the root element
     * @return Root element
     */
    public T getRoot();

    /**
     * Returns true if this binary tree is empty and false otherwise
     * @return if is empty
     */
    public boolean isEmpty();

    /**
     * Returns the number of elements in this binary tree
     * @return size
     */
    public int size();

    /**
     * Returns true if the binary tree contains an element that matches
     * the specified element and false otherwise
     * @param targetElement Element
     * @return true if did find it otherwise return false
     */
    public boolean contains (T targetElement);

    /**
     * Returns a reference to the specified element if it is found in
     * this binary tree.  Throws an exception if the specified element
     * is not found
     * @param targetElement Element
     * @return Element that was trying to find
     * @throws NotFoundException When isn't found
     */
    public T find (T targetElement) throws NotFoundException;

    /**
     * Returns the string representation of the binary tree
     * @return string
     */
    public String toString();

    /**
     * Performs an inorder traversal on this binary tree by calling an
     * overloaded, recursive inorder method that starts with the root
     *
     * @return iterator
     */
    public Iterator<T> iteratorInOrder();

    /**
     * Performs a preorder traversal on this binary tree by calling an
     *  overloaded, recursive preorder method that starts with the root
     * @return iterator
     */
    public Iterator<T> iteratorPreOrder();

    /**
     * Performs a postorder traversal on this binary tree by calling an
     * overloaded, recursive postorder method that starts with the root
     * @return iterator
     */
    public Iterator<T> iteratorPostOrder();

    /**
     * Performs a levelorder traversal on the binary tree, using a queue
     * @return iterator
     * @throws EmptyCollectionException When collection is empty
     */
    public Iterator<T> iteratorLevelOrder() throws EmptyCollectionException;
}
