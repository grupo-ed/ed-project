package Tree;

/**
 * Represents a node in a binary tree with a left and right child
 * @param <T> Specified by the user
 */
public class BinaryTreeNode<T> {

    /**
     * Element that is kept in this node
     */
    private T element;

    /**
     * Path to the right and to the left, branchs below
     */
    private BinaryTreeNode<T> left, right;

    /**
     * Creates a new tree node with the specified data
     * @param obj Object to be added
     */
    public BinaryTreeNode(T obj)
    {
        element = obj;
        left = null;
        right = null;
    }

    /**
     * Returns the number of non-null children of this node.
     *
     * @return number of children
     */
    public int numChildren()
    {
        int children = 0;

        if (left != null)
            children = 1 + left.numChildren();

        if (right != null)
            children = children + 1 + right.numChildren();

        return children;
    }

    public T getElement() {
        return element;
    }

    public void setElement(T element) {
        this.element = element;
    }

    public BinaryTreeNode<T> getLeft() {
        return left;
    }

    public void setLeft(BinaryTreeNode<T> left) {
        this.left = left;
    }

    public BinaryTreeNode<T> getRight() {
        return right;
    }

    public void setRight(BinaryTreeNode<T> right) {
        this.right = right;
    }
}
