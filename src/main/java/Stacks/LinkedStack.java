package Stacks;

import Exceptions.EmptyCollectionException;
import Nodes.LinearNode;

/**
 * Linked Stack
 *
 * @param <T> Specified by the user
 */
public class LinkedStack<T> implements StackADT<T>{

    /**
     * Amount of Nodes
     */
    private int count;

    /**
     * Node on top of stack
     */
    private LinearNode<T> top;

    /**
     * Constructor method
     */
    public LinkedStack() {
        this.top=null;
        this.count=0;
    }

    /**
     * {@inheritDoc}
     *
     * @param element element to be pushed onto stack
     */
    @Override
    public void push(T element) {
        LinearNode<T> temp = new LinearNode<T> (element);

        temp.setNext(top);
        top = temp;
        count++;
    }

    /**
     * {@inheritDoc}
     *
     * @return element removed
     * @throws EmptyCollectionException When Collection is empty
     */
    @Override
    public T pop() throws EmptyCollectionException{
        if (isEmpty()) {
            throw new EmptyCollectionException();
        }

        T element = top.getElement();
        top = top.getNext();
        count--;

        return element;
    }

    /**
     * {@inheritDoc}
     *
     * @return element of top
     * @throws EmptyCollectionException When Collection is empty
     */
    @Override
    public T peek() throws EmptyCollectionException{
        if (isEmpty()) {
            throw new EmptyCollectionException();
        }

        return top.getElement();
    }

    /**
     * {@inheritDoc}
     *
     * @return if is empty
     */
    @Override
    public boolean isEmpty() {
        return (count == 0);
    }

    /**
     * {@inheritDoc}
     *
     * @return size
     */
    @Override
    public int size() {
        return count;
    }

    /**
     * {@inheritDoc}
     *
     * @return representation of queue in one String
     */
    @Override
    public String toString() {
        String result = "";
        LinearNode current = top;

        while (current != null) {
            result += (current.getElement()).toString() + "\n";
            current = current.getNext();
        }

        return result;
    }
}

