package Nodes;

/**
 * Node for Linear implementation
 *
 * @param <T> Specify by the user
 */
public class LinearNode<T> implements LinearNodeADT<T>{

    /**
     * reference to next node in list
     */
    private LinearNode<T> next;

    /**
     * element stored at this node
     */
    private T element;

    /**
     * Creates an empty node.
     */
    public LinearNode() {
        next = null;
        element = null;
    }

    /**
     * Creates a node storing the specified element.
     *
     * @param elem element to be stored
     */
    public LinearNode(T elem) {
        next = null;
        element = elem;
    }

    /**
     * Get next linear node
     * @return Next Node
     */
    @Override
    public LinearNode<T> getNext() {
        return next;
    }

    /**
     * Set next linear node
     * @param node node to follow this one
     */
    @Override
    public void setNext(LinearNode<T> node) {
        next = node;
    }

    /**
     * Set element
     *
     * @param elem element to be stored at this node
     */
    @Override
    public void setElement(T elem) {
        element = elem;
    }

    /**
     * Returns the element stored in this node.
     *
     * @return T element stored at this node
     */
    public T getElement() {
        return element;
    }
}
