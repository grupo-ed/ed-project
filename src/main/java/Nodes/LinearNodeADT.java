package Nodes;

/**
 * Interface of Linear Node
 *
 * @param <T> Specify by the user
 */
public interface LinearNodeADT<T> {
    /**
     * Returns the node that follows this one.
     *
     * @return LinearNode reference to next node
     */
    public LinearNode<T> getNext();

    /**
     * Sets the node that follows this one.
     *
     * @param node node to follow this one
     */
    public void setNext(LinearNode<T> node);

    /**
     * Sets the element stored in this node.
     *
     * @param elem element to be stored at this node
     */
    public void setElement(T elem);
}
