package Lists;

public class ArrayOrderedList<T> extends ArrayList<T> implements OrderedListADT<T> {
    /**
     * Creates an empty list with default size
     */
    public ArrayOrderedList() {
        super();
    }

    /**
     * Creates an empty list with a specified inital size
     * @param initialCapacity
     */
    public ArrayOrderedList (int initialCapacity) {
        super(initialCapacity);
    }

    /**
     * {@inheritDoc}
     * @param element
     */
    public void add (T element) {
        if (size() == arrayList.length) {
            expandCapacity();
        }

        Comparable<T> temp = (Comparable<T>)element;

        int scan = 0;

        while (scan <= rear && temp.compareTo(arrayList[scan]) > 0) {
            scan++;
        }

        for (int scan2=rear+1; scan2 > scan; scan2--) {
            arrayList[scan2] = arrayList[scan2 - 1];
        }

        arrayList[scan] = element;
        rear++;
    }
}
