package Lists;

import Exceptions.*;

/**
 * Interface of Unordered list
 *
 * @param <T> Specified by the user
 */
public interface UnorderedListADT<T> extends ListADT<T>{

    /**
     * Adds the specified element to the front of the list
     *
     * @param element to be added
     */
    public void addToFront(T element);

    /**
     * Adds the specified element to the rear of the list
     *
     * @param element to be added
     */
    public void addToRear(T element);

    /**
     * Adds the specified element after the specified target
     *
     * @param element to be added
     * @param target element to add after
     * @throws NotFoundException When isn't found
     */
    public void addAfter(T element, T target) throws NotFoundException;
}
