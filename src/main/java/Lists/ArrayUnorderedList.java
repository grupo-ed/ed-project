package Lists;

import Exceptions.*;

/**
 * Array Unordered List
 *
 * @param <T> Specified by the user
 */
public class ArrayUnorderedList<T> extends ArrayList<T> implements UnorderedListADT<T>{

    /**
     * Creates an empty list
     */
    public ArrayUnorderedList() {
        super();
    }

    /**
     * Creates an empty list with an specified initial size
     *
     * @param initialSize of the list
     */
    public ArrayUnorderedList(int initialSize) {
        super(initialSize);
    }

    /**
     * {@inheritDoc}
     *
     * @param element to be added
     */
    @Override
    public void addToFront(T element) {
        if(super.size()==arrayList.length) {
            super.expandCapacity();
        }

        T[] temp=(T[]) new Object[arrayList.length];
        System.arraycopy(arrayList, 0, temp, 1, super.size());
        temp[0]=element;

        arrayList=temp;
        rear++;
        modCount++;
    }

    /**
     * {@inheritDoc}
     *
     * @param element to be added
     */
    @Override
    public void addToRear(T element) {
        if(super.size()==arrayList.length) {
            super.expandCapacity();
        }

        arrayList[size()]=element;
        rear++;
        modCount++;
    }

    /**
     * {@inheritDoc}
     *
     * @param element to be added
     * @param target element to add after
     * @throws NotFoundException when target or element isn't found
     */
    @Override
    public void addAfter(T element, T target) throws NotFoundException {
        if(super.size()==arrayList.length) {
            super.expandCapacity();
        }

        if(!super.contains(target)){
            throw new NotFoundException();
        }

        for(int i=size();i>super.find(target)+1;i--) {
            arrayList[i] = arrayList[i - 1];
        }

        arrayList[find(target)+1]=element;
        rear++;
        modCount++;
    }

}
