package Lists;

import Exceptions.*;
import java.util.Iterator;

/**
 * Array List
 *
 * @param <T> Specified by the user
 */
public class ArrayList<T> implements ListADT<T>{

    /**
     * Default Capacity when capacity argument isn't´t given
     */
    private final int DEFAULT_CAPACITY = 100;

    /**
     * Expand ratio
     */
    private final int EXPAND_BY = 2;

    /**
     * Code when object weren't not found
     */
    private final int NOT_FOUND = -1;

    /**
     * Array of object T
     */
    protected T[] arrayList;

    /**
     * Index of object in rear
     */
    protected int rear;

    /**
     * Ammount of modification
     */
    protected int modCount;

    /**
     * {@link Iterator} class
     *
     * @param <T> Specified by the user
     */
    public class BasicIterator<T> implements Iterator<T> {

        private int expectedModcount;
        private int count;

        /**
         * Constructor method
         */
        public BasicIterator() {
            expectedModcount = modCount;
            count = 0;
        }

        /**
         * {@inheritDoc}
         * @return if has a next element or not
         */
        @Override
        public boolean hasNext() {
            try {
                if (expectedModcount != modCount) {
                    throw new CurrentModificationException("Error");
                }
            } catch (CurrentModificationException ex) {
            }
            return (arrayList[count] != null);
        }

        /**
         * {@inheritDoc}
         * @return next element
         */
        @Override
        public T next() {
            try {
                if (expectedModcount != modCount) {
                    throw new CurrentModificationException("Error");
                }
            } catch (CurrentModificationException ex) {
            }
            return (T) arrayList[count++];
        }
    }

    /**
     * Creates an empty list
     */
    public ArrayList() {
        arrayList = (T[]) new Object[DEFAULT_CAPACITY];
        rear = -1;
        modCount = 0;
    }

    /**
     * Creates an empty list with a specified initial size
     *
     * @param initialSize Initial size
     * @throws NegativeArraySizeException When initialSize is negative
     */
    public ArrayList(int initialSize) throws NegativeArraySizeException {
        arrayList = (T[]) new Object[initialSize];
        rear = -1;
        modCount = 0;
    }

    /**
     * {@inheritDoc}
     *
     * @return removed element
     * @throws EmptyCollectionException When Collection is empty
     */
    @Override
    public T removeFirst() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException();
        }

        T temp = arrayList[0];

        for (int i = 0; i < this.size() - 1; i++) {
            arrayList[i] = arrayList[i + 1];
        }

        arrayList[rear] = null;
        rear--;
        modCount++;

        return temp;
    }

    /**
     * {@inheritDoc}
     *
     * @return removed element
     * @throws EmptyCollectionException When Collection is empty
     */
    @Override
    public T removeLast() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException();
        }

        T temp = arrayList[rear];
        arrayList[rear] = null;

        rear--;
        modCount++;

        return temp;
    }

    /**
     * {@inheritDoc}
     *
     * @param element the element to be removed from the list
     * @return removed element
     * @throws EmptyCollectionException When Collection is empty
     * @throws NotFoundException When isn't found
     */
    @Override
    public T remove(T element) throws EmptyCollectionException, NotFoundException {
        if (isEmpty()) {
            throw new EmptyCollectionException();
        }

        if (!contains(element)) {
            throw new NotFoundException();
        }

        for (int i = find(element); i < size() - 1; i++) {
            arrayList[i] = arrayList[i + 1];
        }

        arrayList[rear] = null;
        rear--;
        modCount++;
        return element;
    }

    /**
     * {@inheritDoc}
     *
     * @return first element
     * @throws EmptyCollectionException When Collection is empty
     */
    @Override
    public T first() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException();
        }

        return arrayList[0];
    }

    /**
     * {@inheritDoc}
     *
     * @return element on rear
     * @throws EmptyCollectionException When Collection is empty
     */
    @Override
    public T last() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException();
        }

        return arrayList[rear];
    }

    /**
     * {@inheritDoc}
     *
     * @param target the target that is being sought in the list
     * @return Comparative deference
     */
    @Override
    public boolean contains(T target) {
        return (find(target) != NOT_FOUND);
    }

    /**
     * {@inheritDoc}
     * @return if is empty or not
     */
    @Override
    public boolean isEmpty() {
        return (rear == -1);
    }

    /**
     * Size of array
     * @return int value
     */
    @Override
    public int size() {
        return (rear + 1);
    }

    /**
     * {@inheritDoc}
     * @return iterator
     */
    @Override
    public Iterator<T> iterator() {
        return new BasicIterator<>();
    }

    /**
     * {@inheritDoc}
     * @return representation of array in string
     */
    @Override
    public String toString() {
        String result = "";

        for (int i = 0; i < size(); i++) {
            result += arrayList[i].toString() + "\n";
        }

        return result;
    }

    /**
     * Returns the index of the given element
     *
     * @param element element to find
     * @return index of element
     */
    protected int find(T element) {
        int scan = 0;
        int index = -1;
        boolean found = false;

        if(!isEmpty()) {
            while (!found && scan < size()) {
                if (element.equals(arrayList[scan])) {
                    found = true;
                } else {
                    scan++;
                }
            }

            if (found) {
                index = scan;
            }
        }

        return index;
    }

    /**
     * Expands the size of the ArrayList
     */
    protected void expandCapacity() {
        T[] temp = (T[]) new Object[size()*EXPAND_BY];
        System.arraycopy(arrayList, 0, temp, 0, size());
        arrayList = temp;
    }
}
