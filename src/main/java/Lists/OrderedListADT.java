package Lists;

public interface OrderedListADT<T> extends ListADT<T> {
    /**
     * Adds a specified comparable element to the list, keeping the elements in sorted order
     * @param element to be added
     */
    public void add (T element);
}
