package Exceptions;

/**
 * When the given Index is invalid
 */
public class InvalidIndexException extends Exception {

    /**
     * Creates a new instance of <code>EmptyCollectionException</code> without detail message.
     */
    public InvalidIndexException() {
        System.out.println("Invalid Index");
    }

    /**
     * Constructs an instance of <code>EmptyCollectionException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public InvalidIndexException(String msg) {
        super(msg);
    }
}
