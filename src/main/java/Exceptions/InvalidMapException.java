package Exceptions;

/**
 * When is something related with a Invalid (en existence) Map
 */
public class InvalidMapException extends Exception{

    /**
     * Creates a new instance of <code>EmptyCollectionException</code> without detail message.
     */
    public InvalidMapException() {
        System.out.println("Invalid Map");
    }

    /**
     * Constructs an instance of <code>EmptyCollectionException</code> with the specified detail message.
     * @param msg the detail message.
     */
    public InvalidMapException(String msg) {
        super(msg);
    }
}
