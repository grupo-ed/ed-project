package MapManagement;

import Exceptions.InvalidIndexException;
import Exceptions.InvalidMapException;
import Lists.ArrayUnorderedList;
import org.json.simple.*;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Management of Maps
 */
public class MapManagement extends ArrayUnorderedList<Map> implements MapManagementADT {

    /**
     * The minimum rooms needed to have a valid map
     */
    private static final int MIN_ROOMS = 1;

    /**
     * {@inheritDoc}
     */
    @Override
    public void loadMaps() throws NullPointerException {
        JSONParser parser = new JSONParser();
        File folder = new File("maps/");
        File[] files = folder.listFiles();

        for (File file : files) {
            try (final FileReader reader = new FileReader("maps/" + file.getName())) {
                JSONObject obj = (JSONObject) parser.parse(reader);

                //Reading all map information
                String mapName = (String) obj.get("nome");
                double points = Double.parseDouble(obj.get("pontos").toString());
                JSONArray map = (JSONArray) obj.get("mapa");

                //Checks if the information is valid
                if (mapName.isEmpty() || points <= 0 || map.size() < MIN_ROOMS) {
                    throw new InvalidMapException();
                }

                Iterator mapNamesIterator = this.iterator();

                //Checks if the name of the map already exists and if so adding "(Repetido)"
                while (mapNamesIterator.hasNext()) {
                    if (mapName.equals(((Map) mapNamesIterator.next()).getName())) {
                        mapName += "(Repetido)";
                    }
                }

                //Creating the map
                Map<String> newMap = new Map(mapName, points);

                //Adding all vertices
                for (Object map1 : map) {
                    JSONObject vertex = (JSONObject) map1;
                    newMap.addVertex(vertex.get("aposento").toString());
                }

                //Adding default vertices
                newMap.addVertex("entrada"); //Entry vertex
                newMap.addVertex("exterior"); //Exit vertex

                double maxDamage = 0;

                //Adding edges
                for (Object map1 : map) {
                    JSONObject vertex = (JSONObject) map1;
                    JSONArray connections = (JSONArray) vertex.get("ligacoes");
                    for (Object connection : connections) {
                        //Adding all the edges
                        if (connection.toString().compareTo("exterior") == 0) {
                            newMap.addEdge(vertex.get("aposento").toString(), connection.toString(), 0);
                        } else {
                            newMap.addEdge(connection.toString(), vertex.get("aposento").toString(), Double.parseDouble(vertex.get("fantasma").toString()));
                            maxDamage += Double.parseDouble(vertex.get("fantasma").toString());
                        }
                    }
                }

                //Checks if the map is possible
                if (newMap.shortestPathWeight("entrada", "exterior") < newMap.getPoints()) {

                    addShield(newMap, maxDamage);

                    super.addToRear(newMap);
                }
            } catch (InvalidIndexException | IOException | ParseException | InvalidMapException ex) {
                Logger.getLogger(MapManagement.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void addShield(Map newMap, double maxDamage) throws InvalidIndexException {
        //Generating the shield
        Random random = new Random();
        int randomValue = random.nextInt((int) (maxDamage)) + 1;
        int randomVertex;
        Object[] vertices;
        Iterator iterator;

        boolean added = false;

        do {
            //Creating random vertex index
            randomVertex = random.nextInt((newMap.size() - 2));

            vertices = newMap.getVertices();

            iterator = newMap.possiblePaths((String) vertices[randomVertex]);

            if (iterator.hasNext()) {
                String tempVertex = (String) iterator.next();

                //If there are no ghosts adds the shield
                if (newMap.getWeight(tempVertex, (String) vertices[randomVertex]) == 0) {
                    newMap.addEdge(tempVertex, (String) vertices[randomVertex], randomValue * -1);
                    while (iterator.hasNext()) {
                        newMap.addEdge((String) iterator.next(), (String) vertices[randomVertex], randomValue * -1);
                    }
                    added = true;
                }
            }

            Iterator iteratorEntry = newMap.possiblePaths("entrada");
            while(iteratorEntry.hasNext()){
                if(((String)iteratorEntry.next()).compareTo(String.valueOf(vertices[randomVertex]))==0){
                    newMap.addEdge("entrada", (String) vertices[randomVertex], randomValue * -1);
                }
            }

        } while (!added);
    }
}
