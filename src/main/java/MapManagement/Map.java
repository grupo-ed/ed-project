package MapManagement;

import Exceptions.EmptyCollectionException;
import Exceptions.InvalidIndexException;
import Exceptions.InvalidMapException;
import Game.Player;
import Graphs.Network;
import Lists.ArrayOrderedList;
import Lists.ArrayUnorderedList;

import java.io.*;
import java.util.Iterator;

/**
 * Representative of a Map
 * @param <T> Specified by the user
 */
public class Map<T> extends Network<T> implements MapADT<T>{

    /**
     * Name of the map
     */
    private String name;

    /**
     * Initial points
     */
    private final double points;

    /**
     * Score board
     */
    private ArrayOrderedList scoreBoard;

    /**
     * Create a valid Map
     *
     * @param name is the name of the map
     * @param points are the initial points of the player
     * @throws InvalidMapException when the map is invalid
     */
    public Map(String name, double points) throws InvalidMapException{
        if(name.isEmpty() || points<=0 ){
            throw new InvalidMapException();
        }

        this.name = name;
        this.points = points;
        this.scoreBoard = new ArrayOrderedList();

        this.loadScoreBoards();
    }

    /**
     * Returns the name of the map
     *
     * @return name of the map
     */
    public String getName(){
        return this.name;
    }

    /**
     * Returns the points of the map
     *
     * @return the points
     */
    public double getPoints(){
        return this.points;
    }

    /**
     * Returns the score board
     *
     * @return scoreboard
     */
    public ArrayOrderedList<Player> getScoreBoard() {
        return this.scoreBoard;
    }

    /** * 
     * {@inheritDoc}
     *
     * @param vertex is the vertex specified
     * @return the
     * @throws InvalidIndexException when the index is invalid
     */
    @Override
    public Iterator<T> possiblePaths(T vertex) throws InvalidIndexException {

        //Checking if vertex valid
        if(!indexIsValid(getIndex(vertex))){
            throw new InvalidIndexException();
        }

        ArrayUnorderedList<T> list = new ArrayUnorderedList<>();

        //Adds all adjacent vertex to one array
        for(int i=0;i<numVertices;i++){
            if(adjMatrix[getIndex(vertex)][i]<Double.POSITIVE_INFINITY) {
                list.addToRear(vertices[i]);
            }
        }

        return list.iterator();
    }


    /**
     * {@inheritDoc}
     *
     * @param player is the player to possibly add to the score board
     * @throws EmptyCollectionException when the colection is empty
     */
    public void updateScoreBoard(Player<T> player) throws EmptyCollectionException {
        //Collection can not be empty
        if(!player.getPath().isEmpty()) {
            Player temp = new Player(player.getName(), player.getPoints(), player.getTime());
            scoreBoard.add(temp);
            this.saveScoreBoards();
        } else {
            throw new EmptyCollectionException();
        }
    }

    /**
     * Saves the score board in an Json file
     */
    private void saveScoreBoards() {
        String csvDivisor=";";
        
        new File("scoreBoards/"+this.name).mkdirs(); //Creates a directory for the score boards of the map
        

            //Writing ScoreBoard files
            try (FileWriter file = new FileWriter("scoreBoards/"+ this.name +"/" + this.name.replaceAll("\\s+", "") + "ScoreBoard"+".csv")) {

                file.append("Name");
                file.append(csvDivisor);
                file.append("Points");
                file.append(csvDivisor);
                file.append("Time (ms)");
                file.append('\n');

                Iterator iterator = scoreBoard.iterator();

                //Adding the players information to the file
                while(iterator.hasNext()) {
                    Player temp = (Player) iterator.next();
                    file.append(temp.getName());
                    file.append(csvDivisor);
                    file.append(String.valueOf(temp.getPoints()));
                    file.append(csvDivisor);
                    file.append(String.valueOf(temp.getTime()));
                    file.append('\n');
                }

                file.flush();
                file.close();

            } catch (IOException e) {} 
    }

    /**
     * Loads a score board from an json file
     */
    private void loadScoreBoards() {
        String csvDivisor = ";";
       

            BufferedReader br = null;
            String line = "";

            try (FileReader reader = new FileReader("scoreBoards/" + this.name + "/" + this.name.replaceAll("\\s+", "") + "ScoreBoard" + ".csv")) {

                br = new BufferedReader(reader);

                br.readLine();

                //Reading all the information of the players and saving them to the score board
                while ((line = br.readLine()) != null) {
                    String[] info = line.split(csvDivisor);
                    Player temp = new Player(info[0],Double.valueOf(info[1]),Double.valueOf(info[2]));
                    this.scoreBoard.add(temp);
                }

            } catch (FileNotFoundException e) {} catch (IOException e) {}

    }
}
