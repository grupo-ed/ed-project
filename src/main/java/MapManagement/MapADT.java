package MapManagement;

import Exceptions.InvalidIndexException;
import Game.Player;

import java.util.Iterator;

/**
 * Interface to Implement on Map
 * @param <T> Specified by the user
 */
public interface MapADT<T> {

    /**
     * Returns an iterator with adjacent vertices of a specified vertex
     *
     * @param vertex vertex is the vertex specified
     * @return the iterator with the adjacent vertices
     * @throws InvalidIndexException Invalid Index
     */
    public Iterator<T> possiblePaths(T vertex) throws InvalidIndexException;

}
