package MapManagement;

import Exceptions.InvalidIndexException;

/**
 * Interface of contract to manage maps
 * @param <Map> Need to be Map class
 */
public interface MapManagementADT<Map> {

    /**
     * Loads all maps
     * @throws InvalidIndexException Invalid Index
     */
    public void loadMaps() throws InvalidIndexException;
}
