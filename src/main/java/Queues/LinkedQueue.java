package Queues;

import Nodes.*;
import Exceptions.*;

/**
 * Representation of Linked Queue
 *
 * @param <T> Specify by the user
 */
public class LinkedQueue<T> implements QueueADT<T>{

    /**
     * Amount of Linear Nodes
     */
    private int count;
    private LinearNode<T> front, rear;

    /**
     * Constructor Method of Linked Queue
     */
    public LinkedQueue() {
        count = 0;
        front = rear = null;
    }

    /**
     * {@inheritDoc}
     *
     * @param element the element to be added to the rear of this queue
     */
    public void enqueue (T element) {
        if(element==null){
            throw new NullPointerException();
        }
        LinearNode<T> node = new LinearNode<T>(element);

        if (isEmpty())
            front = node;
        else
            rear.setNext (node);

        rear = node;
        count++;
    }

    /**
     * {@inheritDoc}
     *
     * @return removed object
     * @throws EmptyCollectionException When collection is empty
     */
    public T dequeue() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("Queue");
        }

        T result = front.getElement();
        front = front.getNext();
        count--;

        if (isEmpty()) {
            rear = null;
        }

        return result;
    }

    /**
     * {@inheritDoc}
     *
     * @return element of first node
     * @throws EmptyCollectionException When collection is empty
     */
    public T first() throws EmptyCollectionException {
        if (isEmpty()) {
            throw new EmptyCollectionException("Queue");
        }

        return front.getElement();
    }

    /**
     * {@inheritDoc}
     *
     * @return if is empty or not
     */
    public boolean isEmpty() {
        return (count == 0);
    }

    /**
     * {@inheritDoc}
     *
     * @return size
     */
    public int size() {
        return count;
    }

    /**
     * {@inheritDoc}
     *
     * @return representation of queue in one String
     */
    public String toString() {
        String result = "";
        LinearNode<T> current = front;

        while (current != null) {
            result += (current.getElement()).toString() + "\n";
            current = current.getNext();
        }

        return result;
    }
}
