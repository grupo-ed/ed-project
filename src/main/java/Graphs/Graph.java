package Graphs;

import Exceptions.*;
import Queues.*;
import Lists.*;
import Stacks.*;
import javax.management.InstanceNotFoundException;
import java.util.Iterator;

/**
 * Representative of a Graph
 *
 * @param <T> Specified by the user
 */
public class Graph<T> implements GraphADT<T>{

    /**
     * Default Capacity
     */
    protected final int DEFAULT_CAPACITY = 10;

    /**
     * Number of vertices in the graph
     */
    protected int numVertices;

    /**
     * Adjacency matrix
     */
    protected boolean[][] adjMatrix;

    /**
     * Values of vertices
     */
    protected T[] vertices;

    /**
     * Creates an empty graph.
     */
    public Graph() {
        this.numVertices = 0;
        this.adjMatrix = new boolean[DEFAULT_CAPACITY][DEFAULT_CAPACITY];
        this.vertices = (T[]) (new Object[DEFAULT_CAPACITY]);
    }

    /**
     * {@inheritDoc}
     *
     * @param vertex the vertex to be added to this graph
     */
    @Override
    public void addVertex(T vertex) {
        if (this.numVertices == this.vertices.length) {
            expandCapacity();
        }

        this.vertices[this.numVertices] = vertex;

        //Setting all edges false
        for (int i = 0; i <= this.numVertices; i++) {
            this.adjMatrix[this.numVertices][i] = false;
            this.adjMatrix[i][this.numVertices] = false;
        }

        this.numVertices++;
    }

    /**
     * {@inheritDoc}
     *
     * @param vertex the vertex to be removed from this graph
     */
    @Override
    public void removeVertex(T vertex) throws InvalidIndexException {
        if (!indexIsValid(getIndex(vertex))) {
            throw new InvalidIndexException();
        }

        int index = getIndex(vertex);

        for (int i = index; i < this.numVertices - 1; i++) {
            this.vertices[i] = this.vertices[i + 1];
        }

        //Removing line
        for (int i = index; i < this.numVertices - 1; i++) {
            for (int j = 0; j < this.numVertices; j++) {
                this.adjMatrix[i][j] = this.adjMatrix[i + 1][j];
            }
        }

        //Removing coloumn
        for (int j = index; j < this.numVertices - 1; j++) {
            for (int i = 0; i < this.numVertices - 1; i++) {
                this.adjMatrix[i][j] = this.adjMatrix[i][j + 1];
            }
        }

        this.numVertices--;

    }

    /**
     * {@inheritDoc}
     *
     * @param vertex1 the first vertex
     * @param vertex2 the second vertex
     */
    @Override
    public void addEdge(T vertex1, T vertex2) throws InvalidIndexException {
        if (indexIsValid(getIndex(vertex1)) && indexIsValid(getIndex(vertex2))) {
            this.adjMatrix[getIndex(vertex1)][getIndex(vertex2)] = true;
        } else {
            throw new InvalidIndexException();
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param vertex1 the first vertex
     * @param vertex2 the second vertex
     * @throws InvalidIndexException Invalid index of vertex
     */
    @Override
    public void removeEdge(T vertex1, T vertex2) throws InvalidIndexException{
        if (indexIsValid(getIndex(vertex1)) && indexIsValid(getIndex(vertex2))){
            this.adjMatrix[getIndex(vertex1)][getIndex(vertex2)] = false;
        } else {
            throw new InvalidIndexException();
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param startIndex the starting index
     * @return iterator
     * @throws EmptyCollectionException Empty Collection
     */
    protected Iterator<T> iteratorBFS(int startIndex) throws EmptyCollectionException {
        Integer index = startIndex;
        ArrayUnorderedList<T> resultList = new ArrayUnorderedList<>();
        LinkedQueue<Integer> traversalQueue = new LinkedQueue<>();

        if (indexIsValid(index)) {
            boolean[] visited = new boolean[numVertices];
            for (int i = 0; i < numVertices; i++) {
                visited[i] = false;
            }

            traversalQueue.enqueue(index);
            visited[index] = true;

            while (!traversalQueue.isEmpty()) {
                index = traversalQueue.dequeue();
                resultList.addToRear(vertices[index]);

                //Adding all vertices adjacent to index that have not been visited
                for (int i = 0; i < numVertices; i++) {
                    if (adjMatrix[index][i] && !visited[i]) {
                        traversalQueue.enqueue(i);
                        visited[i] = true;
                    }
                }
            }
        }

        return resultList.iterator();
    }

    /**
     * {@inheritDoc}
     *
     * @param startVertex the starting vertex
     * @return iterator
     * @throws EmptyCollectionException Collection Empty
     */
    @Override
    public Iterator<T> iteratorBFS(T startVertex) throws EmptyCollectionException {
        return iteratorBFS(getIndex(startVertex));
    }

    /**
     * {@inheritDoc}
     *
     * @param startIndex the starting index
     * @return iterator
     * @throws EmptyCollectionException Collection Empty
     */
    protected Iterator<T> iteratorDFS(int startIndex) throws EmptyCollectionException {
        Integer index = startIndex;
        boolean found;
        LinkedStack<Integer> traversalStack = new LinkedStack<>();
        ArrayUnorderedList<T> resultList = new ArrayUnorderedList<>();
        boolean[] visited = new boolean[numVertices];

        if (indexIsValid(index)) {

            for (int i = 0; i < numVertices; i++) {
                visited[i] = false;
            }

            traversalStack.push(index);
            resultList.addToRear(vertices[index]);
            visited[index] = true;

            while (!traversalStack.isEmpty()) {
                index = traversalStack.peek();
                found = false;

                //Adding all vertices adjacent to index that have not been visited
                for (int i = 0; (i < numVertices) && !found; i++) {
                    if (adjMatrix[index][i] && !visited[i]) {
                        traversalStack.push(i);
                        resultList.addToRear(vertices[i]);
                        visited[i] = true;
                        found = true;
                    }
                }
                if (!found && !traversalStack.isEmpty())
                    traversalStack.pop();
            }
        }

        return resultList.iterator();
    }

    /**
     * {@inheritDoc}
     *
     * @param startVertex the starting vertex
     * @return iterator
     * @throws EmptyCollectionException Collection Empty
     */
    @Override
    public Iterator<T> iteratorDFS(T startVertex) throws EmptyCollectionException {
        return iteratorDFS(getIndex(startVertex));
    }

    /**
     * {@inheritDoc}
     *
     * @param startVertex the starting vertex
     * @param targetVertex the ending vertex
     * @return iterator
     * @throws EmptyCollectionException Collection Empty
     */
    @Override
    public Iterator<T> iteratorShortestPath(T startVertex, T targetVertex) throws EmptyCollectionException, InstanceNotFoundException {
        return iteratorShortestPath(getIndex(startVertex),getIndex(targetVertex));
    }

    /**
     * {@inheritDoc}
     *
     * @param startIndex starting index
     * @param targetIndex the ending index
     * @return iterator
     * @throws EmptyCollectionException Collection Empty
     */
    protected Iterator<T> iteratorShortestPath(int startIndex, int targetIndex) throws EmptyCollectionException, InstanceNotFoundException {
        ArrayUnorderedList<T> resultList = new ArrayUnorderedList<>();

        if (indexIsValid(startIndex) && indexIsValid(targetIndex)) {
            Iterator<Integer> it = iteratorShortestPathIndices(startIndex, targetIndex);

            while (it.hasNext()) {
                resultList.addToRear(vertices[((Integer) it.next())]);
            }
        }

        return resultList.iterator();
    }

    /**
     * Returns the iterator with the indices of the vertices that make the shortest path between the tho given index
     *
     * @param startIndex the starting vertex
     * @param targetIndex the target vertex
     * @return iterator with the indices of the vertices that make the shortest path between the tho given index
     * @throws EmptyCollectionException when the collection is empty
     * @throws javax.management.InstanceNotFoundException Instance don´t match
     */
    protected Iterator<Integer> iteratorShortestPathIndices (int startIndex, int targetIndex) throws EmptyCollectionException, InstanceNotFoundException {
        int index = startIndex;
        int[] pathLength = new int[numVertices];
        int[] predecessor = new int[numVertices];

        LinkedQueue<Integer> traversalQueue = new LinkedQueue<>();
        ArrayUnorderedList<Integer> resultList = new ArrayUnorderedList<>();

        if (indexIsValid(startIndex) && indexIsValid(targetIndex) && (startIndex != targetIndex)) {
            boolean[] visited = new boolean[numVertices];
            for (int i = 0; i < numVertices; i++)
                visited[i] = false;

            traversalQueue.enqueue(startIndex);
            visited[startIndex] = true;
            pathLength[startIndex] = 0;
            predecessor[startIndex] = -1;

            while (!traversalQueue.isEmpty() && (index != targetIndex)) {
                index = (traversalQueue.dequeue());

                // Update the pathLength for each unvisited vertex adjacent to index
                for (int i = 0; i < numVertices; i++) {
                    if (adjMatrix[index][i] && !visited[i]) {
                        pathLength[i] = pathLength[index] + 1;
                        predecessor[i] = index;
                        traversalQueue.enqueue(i);
                        visited[i] = true;
                    }
                }
            }

            if (index != targetIndex) {
                return resultList.iterator();
            }

            LinkedStack<Integer> stack = new LinkedStack<>();
            index = targetIndex;
            stack.push(index);

            do {
                index = predecessor[index];
                stack.push(index);
            } while (index != startIndex);

            while (!stack.isEmpty()) {
                resultList.addToRear(stack.pop());
            }
        }

        return resultList.iterator();
    }

    /**
     * {@inheritDoc}
     *
     * @return if is empty or not
     */
    @Override
    public boolean isEmpty() {
        return (size()==0);
    }

    /**
     * {@inheritDoc}
     *
     * @return if is connected or not
     * @throws EmptyCollectionException When collection is empty
     */
    @Override
    public boolean isConnected() throws EmptyCollectionException {
        if (isEmpty())
            return false;

        Iterator<T> it = iteratorBFS(0);
        int count = 0;

        while (it.hasNext()) {
            it.next();
            count++;
        }
        return (count == numVertices);
    }

    /**
     * {@inheritDoc}
     *
     * @return size
     */
    @Override
    public int size() {
        return this.numVertices;
    }

    /**
     * Expands the size of the graph
     */
    protected void expandCapacity() {
        T[] verticesTemp = (T[])(new Object[vertices.length*2]);
        boolean[][] adjTemp = new boolean[vertices.length*2][vertices.length*2];

        for (int i = 0; i < numVertices; i++)
        {
            for (int j = 0; j < numVertices; j++){
                adjTemp[i][j] = adjMatrix[i][j];
            }
            verticesTemp[i] = vertices[i];
        }

        vertices = verticesTemp;
        adjMatrix = adjTemp;
    }

    /**
     * Checks if the index given is valid
     *
     * @param index the index to check
     * @return validation of the index
     */
    protected boolean indexIsValid(int index) {
        return ((index < this.numVertices) && (index >= 0));
    }

    /**
     * Returns the index of the given vertex
     *
     * @param vertex the vertex to get the index
     * @return index of the vertex
     */
    protected int getIndex(T vertex) {
        boolean found = false;
        int index = -1;

        for (int i = 0; !found && i < this.size(); i++) {
            if (this.vertices[i].equals(vertex)) {
                found = true;
                index = i;
            }
        }

        return index;
    }

    /**
     * Return the vertices
     *
     * @return array of vertices
     */
    public T[] getVertices(){
        return this.vertices;
    }
}
