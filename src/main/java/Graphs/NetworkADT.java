package Graphs;

import Exceptions.EmptyCollectionException;
import Exceptions.InvalidIndexException;
import javax.management.InstanceNotFoundException;

/**
 * Interface of {@link Network}
 *
 * @param <T> Specified by the user
 */
public interface NetworkADT<T> extends GraphADT<T> {

    /**
     * Inserts an edge between two vertices
     *
     * @param vertex1 is the first vertex
     * @param vertex2 is the second vertex
     * @param weight of the edge
     * @throws InvalidIndexException Given index is invalid
     */
    public void addEdge (T vertex1, T vertex2, double weight) throws InvalidIndexException;

    /**
     * Returns the weight of the least heavy path between two vertices
     *
     * @param vertex1 is the first vertex
     * @param vertex2 is the target vertex
     * @return weight of the shortest path
     */
    public double shortestPathWeight(T vertex1, T vertex2) throws EmptyCollectionException, InstanceNotFoundException;

}
