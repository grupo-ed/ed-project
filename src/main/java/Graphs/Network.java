package Graphs;

import Exceptions.EmptyCollectionException;
import Exceptions.InvalidIndexException;
import Lists.ArrayUnorderedList;
import Queues.LinkedQueue;
import Stacks.LinkedStack;
import Tree.Heap;

import javax.management.InstanceNotFoundException;
import java.util.Iterator;

/**
 * Representative of {@link Network}
 *
 * @param <T> Specified by the user
 */
public class Network<T> extends Graph<T> implements NetworkADT<T> {

    /**
     * Matrix with the weights
     */
    protected double[][] adjMatrix;

    /**
     * Creates an empty graph
     */
    public Network() {
        super();
        numVertices = 0;
        this.adjMatrix = new double[DEFAULT_CAPACITY][DEFAULT_CAPACITY];
        vertices = (T[]) (new Object[DEFAULT_CAPACITY]);
    }

    /**
     * {@inheritDoc}
     *
     * @param vertex1 is the first vertex
     * @param vertex2 is the second vertex
     * @param weight of the edge
     */
    @Override
    public void addEdge(T vertex1, T vertex2, double weight) throws InvalidIndexException {
        if (!indexIsValid(getIndex(vertex1)) || !indexIsValid(getIndex(vertex2))) {
            throw new InvalidIndexException();
        }
        adjMatrix[getIndex(vertex1)][getIndex(vertex2)] = weight;
    }

    /**
     * Removes an edge between two vertices
     *
     * @param vertex1 the first vertex
     * @param vertex2 the second vertex
     * @throws Exceptions.InvalidIndexException Vertex is invalid
     */
    @Override
    public void removeEdge(T vertex1, T vertex2) throws InvalidIndexException {
        if (indexIsValid(getIndex(vertex1)) && indexIsValid(getIndex(vertex2))) {
            adjMatrix[getIndex(vertex1)][getIndex(vertex2)] = Double.POSITIVE_INFINITY;
        } else {
            throw new InvalidIndexException();
        }
    }

    /**
     * Adds a vertex, associating object with vertex.
     *
     * @param vertex the vertex to be added to this graph
     */
    @Override
    public void addVertex(T vertex) {
        if (numVertices == vertices.length) {
            expandCapacity();
        }

        super.vertices[numVertices] = vertex;
        for (int i = 0; i <= numVertices; i++) {
            adjMatrix[numVertices][i] = Double.POSITIVE_INFINITY;
            adjMatrix[i][numVertices] = Double.POSITIVE_INFINITY;
        }

        numVertices++;
    }

    /**
     * {@inheritDoc}
     *
     * @param startIndex the starting index
     * @return iterator BFS
     * @throws EmptyCollectionException Empty Collection
     */
    @Override
    protected Iterator<T> iteratorBFS(int startIndex) throws EmptyCollectionException {
        Integer index;
        LinkedQueue<Integer> traversalQueue = new LinkedQueue<>();
        ArrayUnorderedList<T> list = new ArrayUnorderedList<>();

        boolean[] visited = new boolean[numVertices];

        if (indexIsValid(startIndex)) {

            //No vertex has been visited yet
            for (int i = 0; i < numVertices; i++) {
                visited[i] = false;
            }

            traversalQueue.enqueue(startIndex);
            visited[startIndex] = true;

            while (!traversalQueue.isEmpty()) {
                index = traversalQueue.dequeue();
                list.addToRear(vertices[index]);

                //Adding all vertices adjacent to index that have not been visited
                for (int i = 0; i < numVertices; i++) {
                    if ((adjMatrix[index][i] < Double.POSITIVE_INFINITY) && !visited[i]) {
                        traversalQueue.enqueue(i);
                        visited[i] = true;
                    }
                }
            }
        }

        return list.iterator();
    }

    /**
     * {@inheritDoc}
     *
     * @param startVertex the starting vertex
     * @return iterator BFS
     * @throws EmptyCollectionException Empty Collection
     */
    @Override
    public Iterator<T> iteratorBFS(T startVertex) throws EmptyCollectionException {
        return iteratorBFS(getIndex(startVertex));
    }

    /**
     * {@inheritDoc}
     *
     * @param startIndex the starting index
     * @return iterator DFS
     * @throws EmptyCollectionException when the collection is empty
     */
    @Override
    protected Iterator<T> iteratorDFS(int startIndex) throws EmptyCollectionException {
        Integer index;
        boolean found;
        LinkedStack<Integer> traversalStack = new LinkedStack<>();
        ArrayUnorderedList<T> list = new ArrayUnorderedList<>();
        boolean[] visited = new boolean[numVertices];

        if (indexIsValid(startIndex)) {

            //No vertex has been visited yet
            for (int i = 0; i < numVertices; i++) {
                visited[i] = false;
            }

            traversalStack.push(startIndex);
            list.addToRear(vertices[startIndex]);
            visited[startIndex] = true;

            while (!traversalStack.isEmpty()) {
                index = traversalStack.peek();
                found = false;

                //Adding all vertices adjacent to index that have not been visited
                for (int i = 0; (i < numVertices) && !found; i++) {
                    if ((adjMatrix[index][i] < Double.POSITIVE_INFINITY) && !visited[i]) {
                        traversalStack.push(i);
                        list.addToRear(vertices[i]);
                        visited[i] = true;
                        found = true;
                    }
                }

                if (!found && !traversalStack.isEmpty()) {
                    traversalStack.pop();
                }
            }
        }

        return list.iterator();
    }

    /**
     * {@inheritDoc}
     *
     * @param startVertex the starting vertex
     * @return iterator DFS
     */
    @Override
    public Iterator<T> iteratorDFS(T startVertex) throws EmptyCollectionException {
        return iteratorDFS(getIndex(startVertex));
    }

    /**
     * Return iterator with the shortest path from a to b
     *
     * @param startVertex vertex initial
     * @param targetVertex vertex final
     * @return iterator with shortest path
     * @throws Exceptions.EmptyCollectionException when the collection is empty
     * @throws javax.management.InstanceNotFoundException Instance is not found
     */
    @Override
    public Iterator<T> iteratorShortestPath(T startVertex, T targetVertex) throws EmptyCollectionException, InstanceNotFoundException {
        if (isEmpty()) {
            throw new EmptyCollectionException("Empty Graph");
        }
        return iteratorShortestPath(getIndex(startVertex), getIndex(targetVertex));
    }

    /**
     * Return iterator with the shortest path from a to b
     *
     * @param startIndex vertex initial
     * @param targetIndex vertex final
     * @return iterator with shortest path
     * @throws Exceptions.EmptyCollectionException when the collection is empty
     * @throws javax.management.InstanceNotFoundException Instance is not found
     */
    @Override
    protected Iterator<T> iteratorShortestPath(int startIndex, int targetIndex) throws EmptyCollectionException, InstanceNotFoundException {

        ArrayUnorderedList<T> resultList = new ArrayUnorderedList<>();
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex)) {
            return resultList.iterator();
        }

        Iterator it = iteratorShortestPathIndices(startIndex, targetIndex);

        return it;
    }

    /**
     * Returns an iterator that contains the indices of the vertices with the shortest path
     * "short" between vertices received
     *
     * @param startIndex initial vertex index
     * @param targetIndex vertex index that aims to reach
     * @throws EmptyCollectionException Collection is empty
     * @return an iterator that contains the indices of the vertices with the shortest path
     */
    @Override
    public Iterator<Integer> iteratorShortestPathIndices(int startIndex, int targetIndex) throws EmptyCollectionException {
        int index;
        double weight;
        int[] predecessor = new int[numVertices];
        Heap<Double> traversalMinHeap = new Heap<>();
        ArrayUnorderedList<Integer> resultList = new ArrayUnorderedList<>();
        LinkedStack<Integer> stack = new LinkedStack<>();

        double[] pathWeight = new double[numVertices];
        for (int i = 0; i < numVertices; i++) {
            pathWeight[i] = Double.POSITIVE_INFINITY;
        }

        boolean[] visited = new boolean[numVertices];
        for (int i = 0; i < numVertices; i++) {
            visited[i] = false;
        }

        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex) || (startIndex == targetIndex) || isEmpty()) {
            return resultList.iterator();
        }

        pathWeight[startIndex] = 0;
        predecessor[startIndex] = -1;
        visited[startIndex] = true;

        //updates the pathWeight of each vertex
        for (int i = 0; i < numVertices; i++) {
            if (!visited[i]) {
                pathWeight[i] = pathWeight[startIndex] + adjMatrix[startIndex][i];
                predecessor[i] = startIndex;
                traversalMinHeap.addElement(pathWeight[i]);
            }
        }

        do {
            weight = traversalMinHeap.removeMin();
            traversalMinHeap.removeAllElements();
            if (weight == Double.POSITIVE_INFINITY) //no possible path
            {
                return resultList.iterator();
            } else {
                index = getIndexOfAdjVertexWithWeightOf(visited, pathWeight, weight);
                visited[index] = true;
            }

            //updates the pathWeight of each vertex
            for (int i = 0; i < numVertices; i++) {
                if (!visited[i]) {
                    if ((adjMatrix[index][i] < Double.POSITIVE_INFINITY) && (pathWeight[index] + adjMatrix[index][i]) < pathWeight[i]) {
                        pathWeight[i] = pathWeight[index] + adjMatrix[index][i];
                        predecessor[i] = index;
                    }
                    traversalMinHeap.addElement(pathWeight[i]);
                }
            }
        } while (!traversalMinHeap.isEmpty() && !visited[targetIndex]);

        index = targetIndex;
        stack.push(index);
        do {
            index = predecessor[index];
            stack.push(index);
        } while (index != startIndex);

        while (!stack.isEmpty()) {
            resultList.addToRear((stack.pop()));
        }
        return resultList.iterator();
    }

    /**
     * Returns the vertex index that is adjacent to the vertex with the index
     * received and the same weight
     *
     * @param visited if true the vertex has already been visited
     * @param pathWeight path weights
     * @param weight lead weight
     * @return vertex index that is adjacent to the vertex with the index
     * Received
     */
    private int getIndexOfAdjVertexWithWeightOf(boolean[] visited, double[] pathWeight, double weight) {
        for (int i = 0; i < numVertices; i++) {
            if ((pathWeight[i] == weight) && !visited[i]) {
                return i;
            }
        }
        System.out.println("Error");
        return -1;
    }

    /**
     * Returns the weight of the lightest path in the network Returns positive infinity
     * if no path is found
     *
     * @param startIndex initial vertex index
     * @param targetIndex target vertex index
     * @return weight of the lightest path in the network or positive infinity if
     * no path is found
     */
    private double shortestPathWeight(int startIndex, int targetIndex) throws EmptyCollectionException, InstanceNotFoundException {
        double result = 0;
        if (!indexIsValid(startIndex) || !indexIsValid(targetIndex)) {
            return Double.POSITIVE_INFINITY;
        }

        int index1, index2;
        Iterator<Integer> it = iteratorShortestPathIndices(startIndex, targetIndex);

        if (it.hasNext()) {
            index1 = it.next();
        } else {
            return Double.POSITIVE_INFINITY;
        }

        while (it.hasNext()) {
            index2 = it.next();
            result += adjMatrix[index1][index2];
            index1 = index2;
        }
        return result;
    }

    /**
     * Returns the weight of the lightest path in the network Returns positive infinity
     * if no path is found
     *
     * @param startVertex initial vertex
     * @param targetVertex target vertex
     * @return weight of the lightest path in the network or positive infinity if
     * no path is found
     */
    @Override
    public double shortestPathWeight(T startVertex, T targetVertex) {
        try {
            return shortestPathWeight(getIndex(startVertex), getIndex(targetVertex));
        } catch (EmptyCollectionException | InstanceNotFoundException ex) {
            throw new NullPointerException("Collection is empty.");
        }
    }

    /**
     * Returns the weight the edge of two vertices
     *
     * @param startVertex is the starting vertex
     * @param targetVertex is the target vertex
     * @return the weight of the edge of two vertices
     * @throws InvalidIndexException when the index is invalid
     */
    public double getWeight(T startVertex, T targetVertex) throws InvalidIndexException {
        if (!indexIsValid(getIndex(startVertex)) || !indexIsValid(getIndex(targetVertex))) {
            throw new InvalidIndexException();
        }
        return adjMatrix[getIndex(startVertex)][getIndex(targetVertex)];
    }

    /**
     * Expands the size of the Network
     */
    @Override
    protected void expandCapacity() {
        T[] verticesTemp = (T[]) (new Object[vertices.length * 2]);
        double[][] adjTemp = new double[vertices.length * 2][vertices.length * 2];

        for (int i = 0; i < numVertices; i++) {
            for (int j = 0; j < numVertices; j++) {
                adjTemp[i][j] = adjMatrix[i][j];
            }
            verticesTemp[i] = vertices[i];
        }

        vertices = verticesTemp;
        adjMatrix = adjTemp;
    }

}
