package Game;

import Exceptions.EmptyCollectionException;
import Exceptions.InvalidIndexException;
import Lists.ArrayUnorderedList;
import MapManagement.Map;

/**
 * Representation of a player
 * @param <T> parametric type
 */
public class Player<T> implements Comparable {

    /**
     * Identification
     */
    private final String name;

    /**
     * Amount of points
     */
    private double points;

    /**
     * Amount of shield points
     */
    private double shieldPoints;
    
    /**
     * If the player has shield
     */
    private boolean pickedShield;

    /**
     * Path traveled by the player
     */
    private ArrayUnorderedList<T> path;

    /**
     * Selected Map
     */
    private Map<T> selectedMap;
    
    /**
     * Time
     */
    private double time;
    

    /**
     * Constructor player method
     *
     * @param name Identification
     * @param selectedMap Selected Map to Play
     */
    public Player(String name, Map selectedMap) {
        this.name = name;
        this.selectedMap = selectedMap;
        this.path = new ArrayUnorderedList<>();
        path.addToRear((T) "entrada");
        this.points = selectedMap.getPoints();
        this.shieldPoints = 0;
        this.pickedShield=false;
    }
    
    /**
     * Constructor player method
     *
     * @param name is the identification
     * @param points are the points of the player
     * @param time is the time of play of the player
     */
    public Player(String name, double points, double time) {
        this.time=time;
        this.name = name;
        this.points = points;
    }

    /**
     * Returns points method
     *
     * @return points
     */
    public double getPoints() {
        return points;
    }

    ;

    /**
     * Returns name method
     *
     * @return name/Identification
     */
    public String getName() {
        return name;
    }

    /**
     * Returns path
     *
     * @return player's path
     */
    public ArrayUnorderedList<T> getPath() {
        return this.path;
    }

    /**
     * Return the current position of the player
     *
     * @return the current position of the player
     * @throws EmptyCollectionException when the collection is empty
     */
    public T getCurrentVertex() throws EmptyCollectionException {
        return path.last();
    }

    /**
     * To change the current map in player
     *
     * @param selectedMap Selected Map to Play
     */
    public void newSelectedMap(Map selectedMap) {
        this.selectedMap = selectedMap;
    }

    /**
     * To keep player Path
     *
     * @param vertex new vertex
     * @throws EmptyCollectionException When map is empty
     * @throws InvalidIndexException When vertex that was given is invalid
     */
    public void goTo(T vertex) throws EmptyCollectionException, InvalidIndexException {
        if (selectedMap.getWeight(path.last(), vertex) < 0 && !pickedShield) {
            shieldPoints -= selectedMap.getWeight(path.last(), vertex);
            this.pickedShield = true; //Disable shield
        } else if(selectedMap.getWeight(path.last(), vertex)>=0){
            if (shieldPoints > 0) {
                shieldPoints -= (selectedMap.getWeight(path.last(), vertex));
                if (shieldPoints < 0) {
                    this.points += shieldPoints;
                    shieldPoints = 0;
                }
            } else {
                this.points -= (selectedMap.getWeight(path.last(), vertex));
            }
        }
        
        path.addToRear(vertex);
        
        if (points <= 0) {
            this.points = 0;
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param cmp Object to compare
     * @return deference value
     */
    @Override
    public int compareTo(Object cmp) {
        //If the players have the same points compares the time of play
        if(((Player) cmp).points!=this.points) {
            return (int) (((Player) cmp).points-this.points);
        } else {
            return (int) (this.time-((Player)cmp).time);
        }
    }

    /**
     * Gets the time
     * @return time
     */
    public double getTime() {
        return time;
    }

    /**
     * Sets the time
     * @param time is the time to set
     */
    public void setTime(double time) {
        this.time = time;
    }

    /**
     * Gets the shield points
     * @return shield points
     */
    public double getShieldPoints() {
        return shieldPoints;
    }

    /**
     * Gets the information if the player already picked a shield
     * @return if the player already picked a shield
     */
    public boolean getPickedShield() {
        return pickedShield;
    }

}
