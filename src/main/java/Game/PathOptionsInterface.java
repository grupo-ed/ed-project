package Game;

import Exceptions.EmptyCollectionException;
import Exceptions.InvalidIndexException;
import MapManagement.Map;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Window of path options
 */
public class PathOptionsInterface {

    /**
     * Selected Map
     */
    private final Map map;

    /**
     * Principal Menu window
     */
    private final JFrame GameInterface;

    /**
     * Frame
     */
    private JFrame frame;

    /**
     * Status Panel
     */
    private JPanel status;

    /**
     * Label to show current Room
     */
    private JLabel currentRoom;

    /**
     * Label to show current Points
     */
    private JLabel currentPoints;

    /**
     * Quit button
     */
    private JButton quitButton;

    /**
     * Player
     */
    private final Player player;


    /**
     * Start playing time
     */
    private final double startTime;

    /**
     * Create a interface with the path options
     *
     * @param map is the current map
     * @param GameInterface is the main interface
     * @param player is the current player
     * @param startTime start time of game
     * @throws EmptyCollectionException When collection is empty
     */
    public PathOptionsInterface(Map map, JFrame GameInterface,Player player,double startTime) throws EmptyCollectionException {
        this.map = map;
        this.GameInterface = GameInterface;
        this.player = player;
        this.startTime = startTime;
        
        initComponents();
    }

    /**
     * Score Board Panel
     */
    private void scoreBoard() {

        JPanel scoreBoard = new JPanel();
        JLabel title = new JLabel("Top 5 Players");
        
        
        String[] columnName = {"Name","Points","Time (ms)"};
        Object[][] data = new Object[map.getScoreBoard().size() + 1][3];
        
        Iterator iterator = map.getScoreBoard().iterator();
        
        data[0][0] = columnName[0];
        data[0][1] = columnName[1];
        data[0][2] = columnName[2];

        //Players information
        for(int i = 1; iterator.hasNext() && i <= 5; i++){
            Player temp = (Player) iterator.next();
            data[i][0] = temp.getName();
            data[i][1] = temp.getPoints();
            data[i][2] = temp.getTime();
        }
        
        //Score table
        JTable table = new JTable();
        table.setSize(700, 100);
        table.setDefaultEditor(Object.class, null);
        table.setModel(new javax.swing.table.DefaultTableModel(data, columnName));

        scoreBoard.add(title);
        scoreBoard.add(table);
        scoreBoard.setBounds(100,150, 300,120);
        scoreBoard.setBackground(Color.lightGray);
        
        frame.add(scoreBoard);
    }

    /**
     * Final menu of the game
     */
    private void gameOver(String message) {        
        //Congratulations Panel
        JPanel congratulations = new JPanel();
        JLabel gameOver = new JLabel(message);
        congratulations.add(gameOver);
        congratulations.setBounds(0,10, 500,25);
        congratulations.setBackground(Color.lightGray);

        //Path Panel
        JPanel pathPanel = new JPanel();
        Iterator iteratorPath = player.getPath().iterator();
        String pathResult="Path";

        int lenght = 70;
        while(iteratorPath.hasNext()){
            if(pathResult.length()>=lenght){
                pathResult += "\n";
                lenght += 70;
            }
            pathResult += " -> "+iteratorPath.next().toString();
        }

        JTextPane pathLabel = new JTextPane();
        pathLabel.setText(pathResult);
        pathLabel.setEditable(false);
        pathPanel.add(pathLabel);
        pathPanel.setBounds(0,35, 500,60);
        pathPanel.setBackground(Color.lightGray);

        //Adding score board
        this.scoreBoard();

        //Adding all panels
        frame.add(congratulations);
        frame.add(pathPanel);
    }

    /**
     * Path option menu
     */
    private void pathOptions(){
        Iterator iterator = null;
        try {
            iterator = map.possiblePaths(player.getCurrentVertex());
        } catch (InvalidIndexException e) {
            System.err.println("Erro a calcular os caminhos possiveis");
        } catch (EmptyCollectionException ex) {
            Logger.getLogger(PathOptionsInterface.class.getName()).log(Level.SEVERE, null, ex);
        }
        int multiplierX = 0;
        int multiplierY = 0;

        while (iterator.hasNext()) {
            String temp = iterator.next().toString();

            if(multiplierX==3){
                multiplierX=0;
                multiplierY++;
            }

            //Creating buttons
            JButton button = new JButton(temp);
            button.setBounds((130 * multiplierX) + 50, (30*multiplierY) + 20, 130, 30);
            multiplierX++;

            button.addActionListener((ActionEvent e) -> {
                try {
                    
                    //When the player founds a ghost or shield
                    if(map.getWeight(player.getCurrentVertex(), temp) > 0 && map.getWeight(player.getCurrentVertex(), temp) < Double.POSITIVE_INFINITY){
                        JOptionPane.showMessageDialog(null, "GHOST \n You found a ghost" + "\n Lost: " + (map.getWeight(player.getCurrentVertex(), temp)) + " points");
                    }else if(map.getWeight(player.getCurrentVertex(), temp)<0 && !player.getPickedShield()){
                        JOptionPane.showMessageDialog(null, "Shield \n You found a shield" + "\n Gain: " + (-map.getWeight(player.getCurrentVertex(), temp)) + " shield points");
                    }
                    
                    player.goTo(temp);
                    
                    new PathOptionsInterface(map, GameInterface,player,startTime);
                    frame.setVisible(false);
                } catch (EmptyCollectionException | InvalidIndexException | HeadlessException ex) {
                }
            });
            frame.add(button);
        }
    }

    /**
     * Final Page of the game
     */
    @SuppressWarnings("empty-statement")
    private void Menu() throws EmptyCollectionException {
        String endGame="";
        if(player.getCurrentVertex().toString().compareTo("exterior")==0) {
            endGame="You won!";
            double playingTime = System.currentTimeMillis() - this.startTime;
            player.setTime(playingTime);
            map.updateScoreBoard(player);;
            
        } else if (player.getPoints()<=0) {
            endGame="You Lost! Better Luck Next Time!";
        }

        //End Game
        if(!endGame.isEmpty()) {
            this.gameOver(endGame);
        } else {
            this.pathOptions();
        }
    }

    /**
     * Method that executes action when Quit button is pressed
     *
     * @param evt
     */
    private void quitButtonActionPerformed(java.awt.event.ActionEvent evt){
        GameInterface.setVisible(true);
        frame.dispose();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     */
    private void initComponents() throws EmptyCollectionException {
        //Initiating the interface
        frame = new JFrame();
        frame.setMinimumSize(new Dimension(500,500));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Setting the status zone
        status = new JPanel();
        status.setBounds(140,400,210,55);
        status.setBackground(Color.lightGray);
        currentRoom = new JLabel("Current Room: "+ player.getCurrentVertex());
        currentPoints = new JLabel("Current Points: " + player.getPoints()+ "  Shield: "+player.getShieldPoints());
        status.add(currentRoom);
        status.add(currentPoints);
        frame.add(status);

        //Creating the quit button
        quitButton = new JButton("Quit");
        quitButton.setBounds(200, 360, 100, 30);
        frame.add(quitButton);
        quitButton.addActionListener((ActionEvent e) -> {
            quitButtonActionPerformed(e);
        });
        
        Menu();

        //Final settings of interface
        frame.setLayout(null);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
